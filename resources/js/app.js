/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
import VueToastr from 'vue-toastr'

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.use(VueToastr)

Vue.component('CompanyDetail',require('./components/CompanyDetail.vue').default);

Vue.component('LegalService',require('./components/LegalService.vue').default);

Vue.component('FinancialPlan',require('./components/FinancialPlan.vue').default);

Vue.component('TechnologyTransfer',require('./components/TechnologyTransfer.vue').default);

Vue.component('Succession',require('./components/Succession.vue').default);
Vue.component('Insurance',require('./components/Insurance.vue').default);
Vue.component('Research',require('./components/Research.vue').default);

Vue.component('Indig',require('./components/Indig.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    data(){
        return {
            form : true,
            isloaded: true,
            registerForm : false,
            isIndividual : false,
            isToggled: false,
            isOperator : false,
            isServiceCompany: false,
            showAccount : "Create Account",
            buttonState: "disabled",
            formData : {
                name:'',
                email:'',
                gender:'',
                category:'',
                phone:'',
                password: '',
                country:'Ghana',
                confirm_password:'',
                registration_number:'',
                tax_identification_number:''
            },
            services : [],
            serviceList : {},
            shareHolderList : {},
            shareholders:[],
            errors:{
                email : '',
                password : '',
                gender : '',
                country : '',
                confirm_password: '',
                phone : '',
                name : ''
            }
        }
    },
    methods : {
        showIndividual(){
            this.showAccount = "Create Individual Account"
            this.form = false;
            this.registerForm = true
            this.isIndividual = true
            this.isOperator = false
            this.formData.category = "individual"
        },
        showService() {
            this.showAccount =  "Operator or Service Company"
            this.isToggled = true
            this.isloaded = false
            
        },

        showService1() {
            this.showAccount =  "Create Service/Operator Account"
            this.form = false
            this.registerForm = true
            this.isIndividual = false
            this.isOperator = true
            this.formData.category ="operator"
        },

        resetForm(){
            this.showAccount = "Create Account"
            this.form = true;
            this.registerForm = false
            this.isIndividual = false
        },

        submitForm(){
            axios.post('/api/v1/register',this.formData).then((resp) => {
                localStorage.setItem("token",resp.data.success)
                window.location = "/dashboard"
            }).catch((resp) => {
                this.errors.email = resp.response.data.error.email[0]
                this.errors.name = resp.response.data.error.name[0]
                this.errors.password = resp.response.data.error.password[0]
                this.errors.country = resp.response.data.error.country[0]
                this.errors.confirm_password = resp.response.data.error.confirm_password[0]
                this.errors.gender = resp.response.data.error.gender[0]
                this.errors.phone = resp.response.data.error.phone[0]

            })
        },
        
        OperatorReg () {
            this.showAccount =  "Operator Account"
            this.form = false
            this.registerForm = true
            this.isIndividual = false
            this.isOperator = true
            this.formData.category ="operator"
        },

        ServiceCompanyReg () {
            this.showAccount =  "Servicing Company Account"
            this.form = false
            this.registerForm = true
            this.isIndividual = false
            this.isServiceCompany = true
            this.formData.category ="service_company"
        }

    }
});
