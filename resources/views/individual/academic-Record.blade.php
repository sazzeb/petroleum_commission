@extends('individual.layout.app')
@section('content-box')
    <div class="row">
        <div class="element-wrapper " style="margin:auto auto">
            <h6 class="element-header">
            
                Academic Details  
            
               
            </h6>
            <div class="element-box" >
                <h5 class="form-header" >
                List Of Acadamic Records
                   
                    <button class="float-right btn btn-primary" data-target="#add" data-toggle="modal" type="button">Add</button>
                </h5>
                <div class="form-desc">
                Easily delete, suspend and change status of a records
                </div>
                <!-- Start Table -->
                
                <div class="table-responsive"   >
                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont dataTable" role="grid" aria-describedby="dataTable1_info" >
                        <thead>
                            <tr>
                                <th style="width: 400px;">Name Of Institution</th>
                                <th style="width: 400px;">Course Of Study</th>
                                <th style="width: 400px;">Educational Level</th>
                                <th style="width: 400px;">Created </th>
                                <th style="width: 200px;" >Action</th>
                                
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 400px;">Name Of Institution</th>
                                <th style="width: 400px;">Course Of Study</th>
                                <th style="width: 400px;">Educational Level</th>
                                <th style="width: 400px;">Created </th>
                                <th style="width: 200px;" >Action</th>
                            </tr>
                        </tfoot>
                        @if(count($academic) >0)
                            @foreach($academic as $academics )
                                <tbody>
                                    <tr>
                                        <td>{{$academics->name_of_institution}}</td>
                                        <td>{{$academics->course_of_study}}</td>
                                        <td>{{$academics->educational_level}}</td>
                                        <td>{{$academics->created_at}}</td>
                                        <td style="display: flex;">
                                            
                                            
                                             <a href="{{'academic'}}/{{$academics->id}}">
                                            <button class="mr-1 mb-1 btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                            </a>
                                            
                                            <button class="btn btn-primary" data-target="#edit-{{$academics->id}}" data-toggle="modal" type="button">View Full</button>
                                        </td>
                                    </tr>
                                    
                                    
                                </tbody>
                            @endforeach
                           
                        @endif
                    </table>
                </div>
                
                <!-- Ends Table -->
                 
            </div>
            <!-- Add Modal -->
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="add" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                Complete Academic Details
                                </h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                                <form action="{{route('academic.store')}}" method="POST" id="form">
                                    @csrf
                                
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Name Of Institution</label><input class="form-control" name="name_of_institution" placeholder="" type="text">
                                            </div>
                                            @if($errors->has('name_of_institution'))
                                                <div class="help-block form-text text-muted form-control-feedback" required>
                                                    {{$errors->first('name_of_institution')}}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Course Of Study</label><input class="form-control" name="course_of_study" placeholder="" type="text" required>
                                            </div>
                                            @if($errors->has('course_of_study'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('course_of_study')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Discipline</label><input class="form-control" name="discipline" placeholder="" type="text" required>
                                            </div>
                                            @if($errors->has('discipline'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('discipline')}}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Educational Level</label><input class="form-control" name="educational_level" placeholder="" type="text" required>
                                            </div>
                                            @if($errors->has('educational_level'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('educational_level')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Country Obtained</label><input class="form-control" name="country_obtained" placeholder="" type="text" required>
                                            </div>
                                            @if($errors->has('country_obtained'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('country_obtained')}}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> Degree Obtained</label><input class="form-control" name="degree_obtained" placeholder="" type="text" required>
                                            </div>
                                            @if($errors->has('degree_obtained'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('degree_obtained')}}
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="">Start Year</label><input class="form-control" name="start_year" placeholder="" type="date" required>
                                            </div>
                                            @if($errors->has('start_year'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('start_year')}}
                                                </div>
                                            @endif
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for=""> End Year</label><input class="form-control" name="end_year" placeholder="" type="date" required>
                                            </div>
                                            @if($errors->has('end_year'))
                                                <div class="help-block form-text text-muted form-control-feedback">
                                                    {{$errors->first('end_year')}}
                                                </div>
                                            @endif
                                        </div>
                                    
                                    </div>
                                    <div class="form-group">
                                        <label for=""> Attach Documents</label><input class="form-control" name="files" placeholder="" type="file" required>
                                    </div>
                                    <div class="form-buttons-w">
                                        <button class="btn btn-primary" type="submit"> Submit</button>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Add Modal -->

            <!-- Edit Modal -->
            @foreach($academic as $academics )
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="edit-{{$academics->id}}" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                Edit Academic Details
                                </h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                            
                            <form action="/academic/{{ $academics->id }}" method="POST" id="form">
                                     @csrf
                                    
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Name Of Institution</label><input class="form-control" name="name_of_institution" placeholder="" type="text" value="{{$academics->name_of_institution}}" required>
                                        </div>
                                        @if($errors->has('name_of_institution'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('name_of_institution')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> Course Of Study</label><input class="form-control" name="course_of_study" placeholder="" type="text" value="{{$academics->course_of_study}}"  required>
                                        </div>
                                        @if($errors->has('course_of_study'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('course_of_study')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Discipline</label><input class="form-control" name="discipline" placeholder="" type="text" value="{{$academics->discipline}}"   required>
                                        </div>
                                        @if($errors->has('discipline'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('discipline')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> Educational Level</label><input class="form-control" name="educational_level" placeholder="" type="text" value="{{$academics->educational_level}}" required>
                                        </div>
                                        @if($errors->has('educational_level'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('educational_level')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Country Obtained</label><input class="form-control" name="country_obtained" placeholder="" type="text" value="{{$academics->country_obtained}}" required>
                                        </div>
                                        @if($errors->has('country_obtained'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('country_obtained')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> Degree Obtained</label><input class="form-control" name="degree_obtained" placeholder="" type="text" value="{{$academics->degree_obtained}}" required>
                                        </div>
                                        @if($errors->has('degree_obtained'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('degree_obtained')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Start Year</label><input class="form-control" name="start_year" placeholder="" type="date"  value="{{$academics->start_year}}" required>
                                        </div>
                                        @if($errors->has('start_year'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('start_year')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> End Year</label><input class="form-control" name="end_year" placeholder="" type="date" value="{{$academics->end_year}}" required>
                                        </div>
                                        @if($errors->has('end_year'))
                                            <div class="help-block form-text text-muted form-control-feedback">
                                                {{$errors->first('end_year')}}
                                            </div>
                                        @endif
                                    </div>
                                
                                </div>
                                <div class="form-group">
                                    <label for=""> Attach Documents {{$academics->files}}</label><input class="form-control" name="files" placeholder="" type="file"  value="{{$academics->files}}" required>
                                </div>
                                <div class="form-buttons-w">
                                    <button class="btn btn-primary" type="submit"> Update</button>
                                </div>
                                </form>

                            
                            <!-- End Update Form -->
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- End Edit Modal -->
        </div>
    </div>
@endsection