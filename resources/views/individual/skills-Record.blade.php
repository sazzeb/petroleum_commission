@extends('individual.layout.app')
@section('content-box')
    <div class="row">
    <div class="element-wrapper " style="margin:auto auto">
            <h6 class="element-header">
            
            Skills Acquired Details
            
               
            </h6>
            <div class="element-box" >
                <h5 class="form-header" >
                List Of Skills Acquired Records
                   
                    <button class="float-right btn btn-primary" data-target="#add" data-toggle="modal" type="button">Add</button>
                </h5>
                <div class="form-desc">
                Easily delete, suspend and change status of a records
                </div>
                <!-- Start Table -->
                
                <div class="table-responsive"   >
                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont dataTable" role="grid" aria-describedby="dataTable1_info" >
                        <thead>
                            <tr>
                                <th style="width: 400px;">Skill Category</th>
                                <th style="width: 400px;">Skill Level</th>
                                <th style="width: 400px;">Description</th>
                                <th style="width: 400px;">Year Obtained </th>
                                <th style="width: 400px;">Created </th>
                                <th style="width: 200px;" >Action</th>
                                
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 400px;">Organisation Name</th>
                                <th style="width: 400px;">Job Title</th>
                                <th style="width: 400px;">Industry</th>
                                <th style="width: 400px;">Year Obtained</th>
                                <th style="width: 400px;">Created </th>
                                <th style="width: 200px;" >Action</th>
                                
                            </tr>
                        </tfoot>
                        @foreach($skill as $skills )
                            <tbody>
                                <tr>
                                    <td>{{$skills->skill_category}}</td>
                                    <td>{{$skills->skill_level}}</td>
                                    <td>{{$skills->description}}</td>
                                    <td>{{$skills->year_obtained}}</td>
                                    <td>{{$skills->created_at}}</td>
                                    <td style="display: flex;">
                                        
                                        
                                            <a href="{{'skills'}}/{{$skills->id}}">
                                        <button class="mr-1 mb-1 btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                        </a>
                                        
                                        <button class="btn btn-primary" data-target="#edit-{{$skills->id}}" data-toggle="modal" type="button">View Full</button>
                                    </td>
                                </tr>
                                
                                
                            </tbody>
                        @endforeach
                           
                     
                    </table>
                </div>
                
                <!-- Ends Table -->
                 
            </div>
            <!-- Add Modal -->
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="add" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                Complete Skills Acquired Details
                                </h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                        <form action="{{route('skills.store')}}" method="POST" id="form">
                            @csrf
                            <h5 class="form-header">
                               
                            </h5>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="">Skill Category</label>
                                        <select name="skill_category" class="form-control">
                                        <option value="">Select </option>
                                            <option value="Mining">Mining </option>
                                            <option value="Drilling">Drilling </option>
                                            <option value="Welding">Welding</option>
                                            <option value="Pipeing">Pipeing</option>
                                        </select>
                                    </div>
                                    @if($errors->has('skill_category'))
                                        <div class="help-block form-text text-muted form-control-feedback" required>
                                            {{$errors->first('skill_category')}}
                                        </div>
                                    @endif
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for=""> Skill Level</label>
                                        <select name="skill_level" class="form-control">
                                        <option value="">Select </option>
                                            <option value="1">Novice</option>
                                            <option value="2">Good</option>
                                            <option value="3">Very Good</option>
                                            <option value="4">Excellent</option>
                                        </select>
                                    </div>
                                    @if($errors->has('skill_level'))
                                        <div class="help-block form-text text-muted form-control-feedback" required>
                                            {{$errors->first('skill_level')}}
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Description</label>
                                <textarea class="form-control" name="description" rows="2"  required> </textarea>
                            </div>
                            @if($errors->has('description'))
                                <div class="help-block form-text text-muted form-control-feedback" required>
                                    {{$errors->first('description')}}
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">Year Obtained</label><input class="form-control" name="year_obtained" placeholder="" type="text">
                                    </div>
                                </div>
                                @if($errors->has('year_obtained'))
                                <div class="help-block form-text text-muted form-control-feedback" required>
                                    {{$errors->first('year_obtained')}}
                                </div>
                            @endif
                            </div>
                            <div class="form-buttons-w">
                                <button class="btn btn-primary" type="submit"> Submit</button>
                            </div>
                        </form>
                               
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Add Modal -->

            <!-- Edit Modal -->
            @foreach($skill as $skills )
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="edit-{{$skills->id}}" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                Edit Skills Acquired Details
</h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="/skills-update/{{ $skills->id }}" method="POST" id="form">
                                @csrf
                                <h5 class="form-header">
                                
                                </h5>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Skill Category</label>
                                            <select name="skill_category" class="form-control">
                                            <option value="{{$skills->skill_category}}">{{$skills->skill_category}}</option>
                                                <option value="Mining">Mining </option>
                                                <option value="Drilling">Drilling </option>
                                                <option value="Welding">Welding</option>
                                                <option value="Pipeing">Pipeing</option>
                                            </select>
                                        </div>
                                        @if($errors->has('skill_category'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('skill_category')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> Skill Level</label>
                                            <select name="skill_level" class="form-control">
                                            <option value="{{$skills->skill_level}}"> {{$skills->skill_level}}</option>
                                                <option value="1">Novice</option>
                                                <option value="2">Good</option>
                                                <option value="3">Very Good</option>
                                                <option value="4">Excellent</option>
                                            </select>
                                        </div>
                                        @if($errors->has('skill_level'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('skill_level')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea class="form-control" name="description" rows="2"  required>{{$skills->description}} </textarea>
                                </div>
                                @if($errors->has('description'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('description')}}
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Year Obtained</label><input class="form-control" name="year_obtained" placeholder="" type="text" value="{{$skills->description}}" >
                                        </div>
                                    </div>
                                    @if($errors->has('year_obtained'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('year_obtained')}}
                                    </div>
                                @endif
                                </div>
                                <div class="form-buttons-w">
                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- End Edit Modal -->
        </div>
    </div>
@endsection