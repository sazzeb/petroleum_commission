@extends('individual.layout.app')
@section('content-box')
    <div class="row">
        <div class="element-wrapper " style="margin:auto auto">
            <h6 class="element-header">
            
                Job Experience Details
            
               
            </h6>
            <div class="element-box" >
                <h5 class="form-header" >
                List Of Job Experience Records
                   
                    <button class="float-right btn btn-primary" data-target="#add" data-toggle="modal" type="button">Add</button>
                </h5>
                <div class="form-desc">
                Easily delete, suspend and change status of a records
                </div>
                <!-- Start Table -->
                
                <div class="table-responsive"   >
                    <table id="dataTable1" width="100%" class="table table-striped table-lightfont dataTable" role="grid" aria-describedby="dataTable1_info" >
                        <thead>
                            <tr>
                                <th style="width: 400px;">Organisation Name</th>
                                <th style="width: 400px;">Job Title</th>
                                <th style="width: 400px;">Industry</th>
                                <th style="width: 400px;">Created </th>
                                <th style="width: 200px;" >Action</th>
                                
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th style="width: 400px;">Organisation Name</th>
                                <th style="width: 400px;">Job Title</th>
                                <th style="width: 400px;">Industry</th>
                                <th style="width: 400px;">Created </th>
                                <th style="width: 200px;" >Action</th>
                                
                            </tr>
                        </tfoot>
                        @foreach($job as $jobs )
                            <tbody>
                                <tr>
                                    <td>{{$jobs->organisation_name}}</td>
                                    <td>{{$jobs->job_title}}</td>
                                    <td>{{$jobs->industry}}</td>
                                    <td>{{$jobs->created_at}}</td>
                                    <td style="display: flex;">
                                        
                                        
                                            <a href="{{'jobs'}}/{{$jobs->id}}">
                                        <button class="mr-1 mb-1 btn btn-danger"><i class="fa fa-trash-o"></i></button>
                                        </a>
                                        
                                        <button class="btn btn-primary" data-target="#edit-{{$jobs->id}}" data-toggle="modal" type="button">View Full</button>
                                    </td>
                                </tr>
                                
                                
                            </tbody>
                        @endforeach
                           
                     
                    </table>
                </div>
                
                <!-- Ends Table -->
                 
            </div>
            <!-- Add Modal -->
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="add" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                Complete Job Experience Details
                                </h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{route('jobs.store')}}" method="POST" id="form">
                                @csrf
                                <h5 class="form-header">
                                </h5>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Organisation Name</label><input class="form-control" name="organisation_name" placeholder="" type="text" required>
                                        </div>
                                        @if($errors->has('organisation_name'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('organisation_name')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> Job Title</label><input class="form-control" name="job_title" placeholder="" type="text" required>
                                        </div>
                                        @if($errors->has('job_title'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('job_title')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Industry</label><input class="form-control" name="industry" placeholder="" type="text" required>
                                        </div>
                                        @if($errors->has('industry'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('industry')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Country Of Employmemnt</label><input class="form-control" name="country_employmemnt" placeholder="" type="text" required>
                                        </div>
                                        @if($errors->has('country_employmemnt'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('country_employmemnt')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for=""> Organisation Address</label><input class="form-control" name="organisation_address" placeholder="" type="text" required>
                                </div>
                                @if($errors->has('organisation_address'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('organisation_address')}}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for=""> Job Description</label>
                                    <textarea class="form-control" name="job_description" rows="2" required></textarea>
                                </div>
                                @if($errors->has('job_description'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('job_description')}}
                                    </div>
                                @endif
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Start Year</label><input class="form-control" name="start_date" placeholder="" type="date" required>
                                        </div>
                                        @if($errors->has('start_date'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('start_date')}}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> End Year</label><input class="form-control" name="end_date" placeholder="" type="date" required>
                                        </div>
                                        @if($errors->has('end_date'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('end_date')}}
                                            </div>
                                        @endif
                                    </div>
                                
                                </div>
                                <div class="form-group">
                                    <label for=""> Attach Documents</label><input class="form-control" name="files" placeholder="" type="file">
                                </div>
                                @if($errors->has('files'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('files')}}
                                    </div>
                                @endif
                                <div class="form-buttons-w">
                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Add Modal -->

            <!-- Edit Modal -->
            @foreach($job as $jobs )
            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="edit-{{$jobs->id}}" role="dialog" tabindex="-1">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                Edit Job Experience Details
                                </h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                        </div>
                        <div class="modal-body">
                            <form action="/jobs/{{ $jobs->id }}" method="POST" id="form">
                                @csrf
                                <h5 class="form-header">
                                </h5>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Organisation Name</label><input class="form-control" name="organisation_name" placeholder="" value="{{$jobs->organisation_name}}" type="text" required>
                                        </div>
                                        @if($errors->has('organisation_name'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('organisation_name')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> Job Title</label><input class="form-control" name="job_title" placeholder="" type="text" value="{{$jobs->job_title}}" required>
                                        </div>
                                        @if($errors->has('job_title'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('job_title')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Industry</label><input class="form-control" name="industry" placeholder="" type="text" value="{{$jobs->industry}}" required>
                                        </div>
                                        @if($errors->has('industry'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('industry')}}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Country Of Employmemnt</label><input class="form-control" name="country_employmemnt" placeholder="" type="text" value="{{$jobs->country_employmemnt}}" required>
                                        </div>
                                        @if($errors->has('country_employmemnt'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('country_employmemnt')}}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for=""> Organisation Address</label><input class="form-control" name="organisation_address" placeholder="" type="text" value="{{$jobs->organisation_address}}"  required>
                                </div>
                                @if($errors->has('organisation_address'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('organisation_address')}}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for=""> Job Description</label>
                                    <textarea class="form-control" name="job_description" rows="2"  required> {{$jobs->job_description}}</textarea>
                                </div>
                                @if($errors->has('job_description'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('job_description')}}
                                    </div>
                                @endif
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Start Year</label><input class="form-control" name="start_date" placeholder="" type="date" value="{{$jobs->start_date}}" required>
                                        </div>
                                        @if($errors->has('start_date'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('start_date')}}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for=""> End Year</label><input class="form-control" name="end_date" placeholder="" type="date" value="{{$jobs->end_date}}" required>
                                        </div>
                                        @if($errors->has('end_date'))
                                            <div class="help-block form-text text-muted form-control-feedback" required>
                                                {{$errors->first('end_date')}}
                                            </div>
                                        @endif
                                    </div>
                                
                                </div>
                                <div class="form-group">
                                    <label for=""> Attach Documents</label><input class="form-control" name="files" placeholder="" type="file">
                                </div>
                                @if($errors->has('files'))
                                    <div class="help-block form-text text-muted form-control-feedback" required>
                                        {{$errors->first('files')}}
                                    </div>
                                @endif
                                <div class="form-buttons-w">
                                    <button class="btn btn-primary" type="submit"> Submit</button>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- End Edit Modal -->
        </div>
    </div>
@endsection