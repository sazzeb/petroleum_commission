@extends('operator.layout.app')
@section('content-box')
<div class="row">
    <div class="element-wrapper col-10" style="margin:auto auto"><h6 class="element-header">Complete Financial Plan Details </h6>

    <financial-plan :financies_detail = "{{ json_encode($financial_detail) }}" :user="{{ auth()->user() }}" :uploads="{{ json_encode($uploads) }}"></financial-plan>
    </div>
</div>
@endsection


