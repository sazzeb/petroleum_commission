@extends('operator.layout.app')
@section('content-box')
    <div class="element-wrapper">
        <h6 class="element-header">
            Editable Tables
        </h6>
        <div class="element-box">
            <h5 class="form-header">
                Manage Employee Details

                <button class="float-right btn btn-success" data-target="#preview" data-toggle="modal" type="button" style="margin: 0px 4px" >Preview</button>

                <button class="float-right btn btn-primary" data-target="#add" data-toggle="modal" type="button">Add</button>
                
            </h5>
            <div class="form-desc">
                Easily delete, suspend and change status of a employee
            </div>
            <div class="table-responsive">
                <div id="dataTable1_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="dataTable1" width="100%" class="table table-striped table-lightfont dataTable"
                                   role="grid" aria-describedby="dataTable1_info" style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th class="sorting_asc" tabindex="0" aria-controls="dataTable1" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="Name: activate to sort column descending" style="width: 400px;">Name
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Position: activate to sort column ascending" style="width: 300px;">
                                        Position
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Age: activate to sort column ascending" style="width: 100px;">Nationality
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Start date: activate to sort column ascending"
                                        style="width: 300px;">Job Description
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Start date: activate to sort column ascending"
                                        style="width: 114px;">Expertise
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Salary: activate to sort column ascending" style="width: 109px;">
                                        Gender
                                    </th>
                                    <!-- <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Salary: activate to sort column ascending" style="width: 109px;">
                                        Rank
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Salary: activate to sort column ascending" style="width: 109px;">
                                        Salary
                                    </th>
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Salary: activate to sort column ascending" style="width: 109px;">
                                        Department
                                    </th> -->
                                    <th class="sorting" tabindex="0" aria-controls="dataTable1" rowspan="1" colspan="1"
                                        aria-label="Salary: activate to sort column ascending" style="width: 109px;">
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th rowspan="1" colspan="1">Name</th>
                                    <th rowspan="1" colspan="1">Position</th>
                                    <th rowspan="1" colspan="1">Nationality</th>
                                    <th rowspan="1" colspan="1">Job Description</th>
                                    <th rowspan="1" colspan="1">Expertise</th>
                                    <th rowspan="1" colspan="1">Gender</th>
                                    <!-- <th rowspan="1" colspan="1">Rank</th>
                                    <th rowspan="1" colspan="1">Salary</th>
                                    <th rowspan="1" colspan="1">Department</th>
                                    <th rowspan="1" colspan="1">Action</th> -->
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($employments as $employee)
                                    <tr role="row" class="even">
                                        <td><a>{{$employee->name}}</a></td>
                                        <td>{{$employee->position}}</td>
                                        <td>{{$employee->nationality}}</td>
                                        <td>{{$employee->job_description}}</td>
                                        <td>{{$employee->expertise}}</td>
                                        <td>{{$employee->gender}}</td>
                                        <!-- <td>{{$employee->rank_category}}</td>
                                        <td>{{$employee->salary}}</td>
                                        <td>{{$employee->department}}</td> -->

                                        <td style="display: flex;">
                                            <button class="mr-1 mb-1 btn btn-danger" onclick="document.getElementById('delete-{{$employee->id}}').submit()"><a><i class="fa fa-trash-o"></i></a></button>
                                            <button class="btn btn-primary" data-target="#edit-{{$employee->id}}" data-toggle="modal" type="button">Edit</button>
                                            <button class="float-right btn btn-primary" data-target="#view-details" data-toggle="modal" type="button">View</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{--Delete form--}}
                            @foreach($employments as $employee)
                                <form action="{{route('employee.destroy',$employee->id)}}" id="delete-{{$employee->id}}" method="POST">
                                    {{method_field('DELETE')}}
                                    @csrf
                                </form>
                                <!-- Ediit Modal -->
                            @endforeach
                            @foreach($employments as $employee)
                                <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="edit-{{$employee->id}}" role="dialog" tabindex="-1">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">
                                                    {{$employee->name}}
                                                </h5>
                                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{route('employee.update',$employee)}}" method="POST" id="form-{{$employee->id}}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for=""> Name </label
                                                        ><input class="form-control" name="name" value="{{$employee->name}}" required placeholder="Enter employee name" type="text">
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Position</label>
                                                                <input class="form-control" value="{{$employee->position}}" required name="position" placeholder="Enter Description " type="text">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Nationality</label>
                                                                <input class="form-control" value="{{$employee->nationality}}" required name="nationality" placeholder="Enter Nationality" type="text">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for=""> Job Description</label>
                                                        <input type="text" class="form-control " value="{{$employee->job_description}}" required name="job_description" placeholder="Enter Job description">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for=""> Expertise</label>
                                                        <input type="text" class="form-control" name="expertise" placeholder="Enter employee expertise" required value="{{$employee->expertise}}">
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Gender</label>
                                                                <input type="text" class="form-control" name="gender" value="{{$employee->gender}}">
                                                            </div>
                                                        </div>
                                                        <!-- <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Department</label>
                                                                <select name="department"  class="form-control">
                                                                    <option value="{{$employee->department}}">{{$employee->department}}</option>
                                                                    <option value="Accounting">Accounting</option>
                                                                    <option value="Development">Development</option>
                                                                    <option value="Engineering">Engineering</option>
                                                                    <option value="Fabrication">Fabrication</option>
                                                                    <option value="Store keeping">Store Keeping</option>
                                                                    <option value="Mining">Mining</option>
                                                                </select>
                                                            </div>
                                                        </div> -->
                                                    </div>
                                                    <!-- <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="">Salary</label>
                                                                <input type="text" class="form-control" name="salary" required value="{{$employee->salary}}">
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label for="rank_category">Rank</label>
                                                                <select name="rank_category"  class="form-control" required>
                                                                    <option value="{{$employee->rank_category}}">{{$employee->rank_category}}</option>
                                                                    <option value="Management">Management</option>
                                                                    <option value="Core Staff">Core Staff</option>
                                                                    <option value="Others">Others</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="button" onclick="document.getElementById('form-{{$employee->id}}').submit()"> Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endforeach
                            {{--End Delete form--}}
                            {{--Add form--}}
                            <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="add" role="dialog" tabindex="-1">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">
                                                Add an Employee Detail
                                            </h5>
                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('employee.store')}}" method="POST" id="add-form">
                                                @csrf
                                                <div class="form-group">
                                                    <label for=""> Name </label
                                                    ><input class="form-control" name="name"  required placeholder="Enter employee name" type="text">
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Position</label>
                                                            <input class="form-control"  required name="position" placeholder="Enter Description " type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Nationality</label>
                                                            <input class="form-control"  required name="nationality" placeholder="Enter Nationality" type="text">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for=""> Job Description</label>
                                                    <input type="text" class="form-control"  required name="job_description" placeholder="Enter Job description">
                                                </div>
                                                <div class="form-group">
                                                    <label for=""> Expertise</label>
                                                    <input type="text" class="form-control" name="expertise" placeholder="Enter employee expertise" required >
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Gender</label>
                                                            <select name="gender" class="form-control">
                                                                <option value="male">Male</option>
                                                                <option value="Female">Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Department</label>
                                                            <select name="department"  class="form-control">
                                                                <option value="Accounting">Accounting</option>
                                                                <option value="Development">Development</option>
                                                                <option value="Engineering">Engineering</option>
                                                                <option value="Fabrication">Fabrication</option>
                                                                <option value="Store keeping">Store Keeping</option>
                                                                <option value="Mining">Mining</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="">Salary</label>
                                                            <input type="text" class="form-control" name="salary" required >
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-group">
                                                            <label for="rank_category">Rank</label>
                                                            <select name="rank_category"  class="form-control" required>
                                                                <option value="Management">Management</option>
                                                                <option value="Core Staff">Core Staff</option>
                                                                <option value="Others">Others</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="float-right">
                                                    <button class="btn btn-secondary" data-dismiss="modal" type="button"> Close</button><button class="btn btn-primary" type="submit"> Save </button>

                                                </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- End Add Modal -->
                            <!-- View Modal -->
                 <div aria-hidden="true" aria-labelledby="exampleModalLabel" class="modal fade" id="view-details" role="dialog" tabindex="-1">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">
                                  Full Details
                                </h5>
                                <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"> &times;</span></button>
                            </div>
                            <div class="modal-body">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End View Modal -->
                    </div>
                </div>
            </div>

    </div>
    </div>
@endsection
@push('scripts')