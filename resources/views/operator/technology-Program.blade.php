@extends('operator.layout.app')
@section('content-box')
<div class="row">  
    <div class="col-sm-12">  
        <div class="element-wrapper">
            <h6 class="element-header">
            Technology Transfer Plan
            </h6>

            <technology-transfer :techs="{{ json_encode($techs) }}" :user="{{ auth()->user() }}"></technology-transfer>
            
            </div>
        </div>
    </div>
</div>
@endsection