@extends('operator.layout.app')
@section('content-box')
<div class="row">
    <div class="element-wrapper col-10" style="margin:auto auto">
      <h6 class="element-header">
        Complete Legal Service Plan Details
      </h6>
      <div class="element-box ">
      <legal-service :user="{{ auth()->user() }}" :uploads="{{ json_encode( $uploads ) }}" :legals="{{ json_encode( $legals ) }}"></legal-service>
      </div>
    </div>
</div>
@endsection