@extends('operator.layout.app')
@section('content-box')
<div class="row">  
    <div class="col-sm-12">  
        <div class="element-wrapper">
            <h6 class="element-header">
            Research And Development Plan
            </h6>
            <research :researches="{{ json_encode($reach) }}" :user="{{ auth()->user() }}"></research>
            
            </div>
        </div>
    </div>
</div>
@endsection