@extends('operator.layout.app')
@section('content-box')
<div class="row">  
    <div class="col-sm-12">  
        <div class="element-wrapper">
            <h6 class="element-header">
            Insurance Service Plan
            </h6>
            <insurance :insured="{{ json_encode($insures)  }} " :user="{{ auth()->user() }}"></insurance>
            </div>
        </div>
    </div>
</div>
@endsection