@extends('operator.layout.app')
@section('content-box')
<div class="row">
<div class="col-lg-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Complete Company Details
      </h6>
      <company-detail :companies="{{ json_encode($company_detail) }}" :user="{{ auth()->user() }}" :uploads="{{ json_encode($uploads) }}"></company-detail>
    </div>
  </div>
</div>
@endsection
