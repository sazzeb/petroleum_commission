@extends('operator.layout.app')
@section('content-box')
<div class="row">  
    <div class="col-sm-12">  
        <div class="element-wrapper">
            <h6 class="element-header">
            Succession Plan
            </h6>
            <succession :successions="{{ json_encode($successions) }}" :user="{{ auth()->user() }}"><succession>
            </div>
        </div>
    </div>
</div>
@endsection