@extends('operator.layout.app')
@section('content-box')
<div class="row">  
    <div class="col-sm-12">  
        <div class="element-wrapper">
            <h6 class="element-header">
            Indeigenous Plan
            </h6>
            <indig :user="{{ auth()->user() }}" :plans="{{ json_encode($plans) }}"></indig>
        </div>
    </div>
</div>
@endsection