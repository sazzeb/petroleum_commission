<div class="menu-w color-scheme-dark color-style-bright menu-position-side menu-side-left menu-layout-compact sub-menu-style-over sub-menu-color-bright selected-menu-color-light menu-activated-on-hover menu-has-selected-link">
    <div class="logo-w">
        <a class="logo" href="index.html">
            <div class="logo-element"></div>
            <div class="logo-label">
               {{config('app.name')}}
            </div>
        </a>
    </div>
    <div class="logged-user-w avatar-inline">
        <div class="logged-user-i">
            <div class="avatar-w">
                <img alt="" src="{{asset('img/avatar1.png')}}">
            </div>
            <div class="logged-user-info-w">
                <div class="logged-user-info-w">
                    <div class="logged-user-name">
                        {{auth()->user()->name}}
                    </div>
                    <div class="logged-user-role">
                        {{auth()->user()->category}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ul class="main-menu">
    <li class="selected">
            <a href="dashboard">
                <div class="icon-w">
                    <div class="os-icon os-icon-layout"></div>
                </div>
                <span>Dashboard</span>
            </a>
        </li>
        <li>
            <a href="company-details">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Company Details</span>
            </a>
        </li>
        <li class="sub-header">
            <span>Local Content</span>
        </li>
        
        <li>
            <a href="{{route('employee.index')}}">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Employee Information</span>
            </a>
        </li>
        <li>
            <a href="succession-plan">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Succession Plan & Regulation</span>
            </a>
        </li>
        <li>
            <a href="research-development-plan">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Research & Dvelopment </span>
            </a>
        </li>
        <li>
            <a href="technology-transfer-program">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Technology Transfer Program</span>
            </a>
        </li>
        <li>
            <a href="insurance-service-plan">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Insurance Service Plan</span>
            </a>
        </li>
        <li>
            <a href="legal-service-plan">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Legal Service Plan</span>
            </a>
        </li>
        <li>
            <a href="financial-plan">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Finanicial Plan</span>
            </a>
        </li>
        
        <li>
            <a href="indigenous-plan">
                <div class="icon-w">
                    <div class="os-icon os-icon-package"></div>
                </div>
                <span>Indigenous Plan</span>
            </a>
        </li>

        <li class="sub-header">
            <span>Categories</span>
        </li>
        <li>
            <a href="">
                <div class="icon-w">
                    <div class="os-icon os-icon-file-text"></div>
                </div>
                <span>Preview All</span>
            </a>
        </li>
    </ul>
</div>