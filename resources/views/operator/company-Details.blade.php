@extends('operator.layout.app')
@section('content-box')
<div class="row">
<div class="col-lg-12">
    <div class="element-wrapper">
      <h6 class="element-header">
        Complete Company Details
      </h6>
      <div class="element-box">
        <form action="{{route('company.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <br>
            <h5> Company Details  </h5>
            <br>
          <div class="form-group">
            <label for=""> Company Name</label><input class="form-control" placeholder="Company Name" type="text">
          </div>
          
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label for=""> Registration Number</label><input class="form-control" placeholder="Registration Number" type="text">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label for="">Tax Identification Number</label><input class="form-control" placeholder="Tax Identification Number" type="text">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Data Of Incorporation</label>
                  <div class="date-input">
                    <input class="single-daterange form-control" placeholder="dd/mm/yyy" type="text" value="">
                  </div>
                </div>
            </div>
                
            <div class="col-sm-6">
                <div class="form-group">
                  <label for=""> Date Of Commencement Of Service</label>
                  <div class="date-input">
                    <input class="single-daterange form-control" placeholder="dd/mm/yyy" type="text" value="">
                  </div>
                </div>
            </div>
          </div>

            <div class="form-group">
                <label for="">Petroleum Commision Permit Number</label><input class="form-control" placeholder="Petroleum Commision Permit Number" type="text">
            </div>
            <button @click="addShareholder" type="button" class="btn mb-3 btn-primary">Add share Holders</button>
            <br>
            <div class="row shareholderParent" v-for="(shareholder,index) in shareholders">
                <div class="col-sm-3">
                    <div class="form-group">
                    <input class="form-control" placeholder="Names" name="shareholder['name'][]" type="text">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                       <input class="form-control" placeholder="Email " name="shareholder['email'][]"type="email">
                    </div>
                </div>

                <div class="col-sm-2">
                    <div class="form-group">
                       <input class="form-control" placeholder="Phone " type="text">
                    </div>
                </div>
                
                <div class="col-sm-2">
                    <div class="form-group">
                    <input class="form-control" placeholder="%" type="text">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                    <label for=""> </label>
                    <button type="button" @click="removeShareholder(index)" class="btn btn-danger btn-md"> Remove</button>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for=""> Ownership</label><input class="form-control" placeholder="Ownership" type="text">
            </div>

            <button type="button" class="btn btn-primary mb-3" @click="addService"> Click to add service</button>
            <br>
            <div class="row"  class="parent" v-for="(service,index) in services">
                <div class="col-sm-8">
                    <div class="form-group">
                    <input class="form-control" placeholder=" Enter Services" type="text">
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                    <label for=""> </label>
                        <button type="button" class="btn btn-danger" id="remove" @click="removeService(index)">Remove</button>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                    <label for="">Postal Aaddress </label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    <label for=""> Telephone</label>
                    <input class="form-control" placeholder="Telephone " type="text">
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                    <label for=""> Website Address</label>
                    <input class="form-control" placeholder="Website Address " type="text">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <div class="form-group">
                    <label for="">Plot Number </label>
                    <input class="form-control" placeholder="#Plot " type="text">
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                    <label for=""> Street Name</label>
                    <input class="form-control" placeholder="Street Name " type="text">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                    <label for=""> City</label>
                    <input class="form-control" placeholder="City " type="text">
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                    <label for=""> Region</label>
                    <input class="form-control" placeholder="Region " type="text">
                    </div>
                </div>
            </div>
            <br>
            <h5>Head Of Company Dtails  </h5>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label for=""> Name Of Head Of Company</label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                    <label for=""> Head Position</label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label for="">Head Phone Number </label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                    <label for=""> Head Email</label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>
            </div>
            <br>
            <h5>Enquire Personal Dtails  </h5>
            <br>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label for="">Enquire Personal Name </label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                    <label for=""> Enquire Personal Email</label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                    <label for="">Enquire Personal Position </label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                    <label for=""> Enquire Personal Business Address</label>
                    <input class="form-control" placeholder=" " type="text">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Enquire Personal  Address</label>
                <input class="form-control" placeholder=" " type="text">
            </div>
            <h7> Attach Company Files </h7>
            <br>
            <div class="row">
            
                <div class="col-sm-12">
                    <div class="form-group">
                        <input class="form-control"  type="file" multiple>
                    </div>
                </div>
            </div>
            <div class="form-buttons-w">
                <button class="btn btn-primary" type="submit"> Submit</button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@push('scripts')
<script src="{{asset('js/app.js')}}"></script>
@endpush
