<!DOCTYPE html>
<html>
<head>
    <title>{{config('app.name')}}</title>
    <meta charset="utf-8">
    <meta content="ie=edge" http-equiv="x-ua-compatible">
    <meta content="template language" name="keywords">
    <meta content="Tamerlan Soziev" name="author">
    <meta content="Admin dashboard html template" name="description">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="favicon.png" rel="shortcut icon">
    <link href="apple-touch-icon.png" rel="apple-touch-icon">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet" type="text/css">
    <link href="{{asset('bower_components/slick-carousel/slick/slick.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="">
    <link href="css/main.css?version=4.4.0" rel="stylesheet">
</head>
<body>
<div class="all-wrapper menu-side with-pattern" id="app">
    <div class="auth-box-w wider">
        <div class="logo-w" style="padding:10%;color:red;">
            @if(Session::has('errors'))
                @if(isset(Session::get('errors')['email'][0]))
                <p>{{Session::get('errors')['email'][0]}}</p>
                    @endif
                @endif
        </div>
        <h4 class="auth-header" v-text="showAccount">
        </h4>

        <form action="{{route('register')}}" method="post">
            @csrf
            <div class="row" style="display: flex;justify-content: space-between;flex-direction: column;" v-if="form">
                
                <div class="load-detail" v-if="isloaded">
                    <button class="btn btn-primary mb-2" @click.prevent="showIndividual">Individual</button>
                        <p class="text-center">Or</p>
                    <button class="btn btn-primary" @click.prevent="showService">Operator or Service Company</button>
                </div>


                <div class="choose_one" v-if="isToggled">
                    <button class="btn btn-primary mb-2" @click.prevent="OperatorReg">Operator</button>
                        <p class="text-center">Or</p>
                    <button class="btn btn-primary" @click.prevent="ServiceCompanyReg">Service Company</button>
                </div>


                <a href="{{route('login')}}" class="mt-3 text-right text-danger">Login</a>
            </div>

            <div v-show="registerForm">
                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" name="name" placeholder="Enter name" required>
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="email" class="form-control" name="email" placeholder="Enter email" required>
                    <div class="help-block form-text text-muted form-control-feedback" v-if="errors" >

                    </div>

                </div>
                <div class="row" v-if="isIndividual">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Gender</label>
                            <select name="gender" class="form-control" required>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                        </div>
                        <input type="hidden" v-if="isIndividual" name="category" value="individual">
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Country</label>
                            <input type="text" class="form-control"  disabled="disabled" name="country" value="Ghana" required>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Phone</label>
                    <input type="text" class="form-control" name="phone" placeholder="Please enter telephone">
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" id="password" class="form-control" placeholder="password" name="password">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Confirm Password</label>
                            <input type="password" id="confirm_password" class="form-control" placeholder="Confirm Password" name="confirm_password">
                        </div>
                    </div>
                </div>
                <div class="row" v-if="isOperator">
                    <input type="hidden" v-if="isOperator" name="category" value="operators">
                    <input type="hidden" v-if="isOperator" name="gender" value="male">
                    <div class="col-sm-6" v-if="isOperator">
                        <div class="form-group">
                            <label for="">Registration Number</label>
                            <input type="text" placeholder="Reg number" name="registration_number" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Tax number</label>
                            <input type="text" placeholder="Tax number" name="tax_identification_number" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class="row" v-if="isServiceCompany">
                    <input type="hidden" v-if="isServiceCompany" name="category" value="service_company">
                    <input type="hidden" v-if="isServiceCompany" name="gender" value="male">
                    <div class="col-sm-6"  v-if="isServiceCompany">
                        <div class="form-group">
                            <label for="">Registration Number</label>
                            <input type="text" placeholder="Reg number" name="registration_number" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Tax number</label>
                            <input type="text" placeholder="Tax number" name="tax_identification_number" class="form-control" required>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Submit</button>
                <button class="btn btn-warning" @click.prevent="resetForm">Cancel</button>
            </div>

        </form>
    </div>
</div>
<script src="{{asset('js/app.js')}}">

</script>
<script>
    var password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>
</body>
