<?php

use Illuminate\Database\Seeder;

class CompanyDetailPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\CompanyDetail', 30)->create();
    }
}
