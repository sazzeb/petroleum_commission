<?php

use Illuminate\Database\Seeder;

class SkillRequiredSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\SkillAcquired', 13)->create();
    }
}
