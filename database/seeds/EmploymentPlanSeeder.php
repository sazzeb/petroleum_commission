<?php

use Illuminate\Database\Seeder;

class EmploymentPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\EmploymmentPlan', 200)->create();
    }
}
