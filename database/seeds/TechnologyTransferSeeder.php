<?php

use Illuminate\Database\Seeder;

class TechnologyTransferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\TechnologyTransferPlan', 13)->create();
    }
}
