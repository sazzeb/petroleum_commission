<?php

use Illuminate\Database\Seeder;

class TrainingSubPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\TrainingSubPlan', 13)->create();
    }
}
