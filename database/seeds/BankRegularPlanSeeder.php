<?php

use Illuminate\Database\Seeder;

class BankRegularPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\BankRegulation', 20)->create();
    }
}
