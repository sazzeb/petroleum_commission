<?php

use Illuminate\Database\Seeder;

class SuccessionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\SuccessionPlan', 13)->create();
    }
}
