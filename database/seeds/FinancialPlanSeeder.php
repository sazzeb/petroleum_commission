<?php

use Illuminate\Database\Seeder;

class FinancialPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\FinancialServicePlan', 13)->create();
    }
}
