<?php

use Illuminate\Database\Seeder;

class JobExperinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\JobExperience', 3)->create();
    }
}
