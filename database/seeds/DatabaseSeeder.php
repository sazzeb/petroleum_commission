<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);
        $this->call(AcademicRecordSeeder::class);
        $this->call(BankRegularPlanSeeder::class);
        $this->call(CompanyDetailPlanSeeder::class);
        $this->call(EmploymentPlanSeeder::class);
        // $this->call(FinancialPlanSeeder::class);
        $this->call(InsurranceServiceSeeder::class);
        $this->call(LegalServiceSeeder::class);
        $this->call(ResearchDevelopmentSeeder::class);
        $this->call(SkillRequiredSeeder::class);
        $this->call(SuccessionPlanSeeder::class);
        $this->call(TechnologyTransferSeeder::class);
        // $this->call(ThirdPartySeeder::class);
        $this->call(TrainingSubPlanSeeder::class);
    }
}
