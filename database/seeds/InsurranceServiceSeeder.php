<?php

use Illuminate\Database\Seeder;

class InsurranceServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\InsuranceServicePlan', 13)->create();
    }
}
