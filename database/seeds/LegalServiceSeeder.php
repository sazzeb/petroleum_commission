<?php

use Illuminate\Database\Seeder;

class LegalServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\LegalServicePlan', 13)->create();
    }
}
