<?php

use Illuminate\Database\Seeder;

class ResearchDevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\ResearchDevelopmentPlan', 13)->create();
    }
}
