<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcademicRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('academic_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('educational_level');
            $table->string('country_obtained');
            $table->string('discipline');
            $table->string('course_of_study');
            $table->string('name_of_institution');
            $table->date('start_year');
            $table->date('end_year');
            $table->string('degree_obtained');
            $table->text('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('academic_records');
    }
}
