<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('company_name');
            $table->string('company_registration_number');
            $table->string('tax_identity_number');
            $table->date('date_of_incorporation');
            $table->date('date_of_commencement_of_service');
            $table->string('petroleum_commision_permit_number');
            $table->text('names_of_shareholder_with_percentage');
            $table->string('ownership');
            $table->string('service_provided');

            $table->string('postal_address');
            $table->string('telephone');
            $table->string('website_address')->nullable();
            $table->string('house_or_plot_number');
            $table->string('street_name');
            $table->string('city');
            $table->string('region');
            $table->string('name_of_head_of_company');
            $table->string('head_position');
            $table->string('head_phone');
            $table->string('head_email')->unique();

            $table->string('enquire_personal_name');
            $table->string('enquire_personal_email')->nullable();
            $table->string('enquire_personal_position');
            $table->string('enquire_personal_business_address');
            $table->string('enquire_personal_address');
            $table->text('files')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_details');
    }
}
