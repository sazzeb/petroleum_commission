<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialServicePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_service_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('name_of_banks');
            $table->text('office_location');
            $table->string('branch');
            $table->date('date_of_engagement');

            $table->text('financial_service_for_pass_six_month')->nullable();
            $table->text('financial_service_for_next_six_month')->nullable();

            $table->text('financial_expenditure_for_pass_six_month')->nullable();
            $table->text('financial_expenditure_for_next_six_month')->nullable();
            
            $table->text('list_of_service_expenditure')->nullable();

            $table->string('name_of_indigenous_bank')->nullable();
            $table->string('indigenous_bank_financial_expenditure')->nullable();
            $table->string('indigenous_bank_date_of_engagement')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_service_plans');
    }
}
