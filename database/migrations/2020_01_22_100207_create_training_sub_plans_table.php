<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingSubPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_sub_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('employee_name');
            $table->string('position');
            $table->string('type_of_training');
            $table->text('detail_of_training');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('training_outcomes');
            $table->integer('cost');
            $table->string('remarks');
            $table->text('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_sub_plans');
    }
}
