<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuccessionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('succession_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('name_of_expatriate');
            $table->string('expatriate_position');
            $table->string('expatraite_certificate');
            $table->string('expatraite_working_experince');
            $table->text('remarks');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('ghanaians_detials');
            $table->text('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('succession_plans');
    }
}
