<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTechnologyTransferPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('technology_transfer_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('initiative_programme');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('implementaion_strategy');
            $table->integer('expenditure_incurred');
            $table->string('jv_expertise_role')->nullable();
            $table->text('jv_ghanaian_role')->nullable();
            $table->text('jv_strategy_for_transfer_of_technology')->nullable();
            $table->text('jv_strategy_for_transfer_of_skill')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('technology_transfer_plans');
    }
}
