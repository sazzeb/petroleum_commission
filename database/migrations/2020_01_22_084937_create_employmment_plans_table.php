<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmploymmentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employmment_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('position');
            $table->string('name');
            $table->string('nationality');
            $table->text('job_description');
            $table->string('expertise');
            $table->string('department');
            $table->integer('salary');
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->enum('rank_category', ['management', 'core_staff', 'others'])->default('others');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employmment_plans');
    }
}
