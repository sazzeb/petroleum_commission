<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThirdPartyStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('third_party_staff', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('agency');
            $table->string('nationality');
            $table->string('name');
            $table->string('job_description');
            $table->string('skill_required');
            $table->string('gender');
            $table->integer('salary')->nullable();
            $table->enum('category', ['ghanaians', 'expatriates', 'others'])->default('ghanaians');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('third_party_staff');
    }
}
