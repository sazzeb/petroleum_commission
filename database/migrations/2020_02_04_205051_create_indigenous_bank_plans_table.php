<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndigenousBankPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indigenous_bank_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_of_banks');
            $table->string('user_unique');
            $table->string('office_location');
            $table->text('list_of_service');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indigenous_bank_plans');
    }
}
