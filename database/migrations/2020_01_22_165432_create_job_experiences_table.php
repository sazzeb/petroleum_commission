<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_experiences', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('organisation_name');
            $table->string('job_title');
            $table->string('industry');
            $table->string('organisation_address');
            $table->string('country_employmemnt');
            $table->text('job_description');
            $table->date('start_date');
            $table->date('end_date');
            $table->text('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_experiences');
    }
}
