<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLegalServicePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_service_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('name_of_firm');
            $table->date('date_of_engagement');
            $table->string('location');
            $table->text('files')->nullable();
            $table->text('past_six_month_obtained')->nullable();
            $table->text('next_six_month_obtained')->nullable();
            $table->text('expenditure_next_six_month');
            $table->text('expenditure_last_six_month');
            $table->text('nature_of_expenditure');
            $table->text('external_solicitors')->nullable();
            $table->text('comprehensive_report')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_service_plans');
    }
}
