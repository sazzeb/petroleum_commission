<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceServicePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_service_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_unique');
            $table->string('name_of_insurance_provider');
            $table->string('office_location');
            $table->text('asset_issued');
            $table->integer('sum_issued');
            $table->string('insurance_cover');
            $table->string('insurance_broker');
            $table->string('name_of_reinsurance_service_provider');
            $table->text('asset_reinsured');
            $table->integer('sum_reinsured');
            $table->text('reason_for_omission')->nullable();
            $table->text('files')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_service_plans');
    }
}
