<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\InsuranceServicePlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(InsuranceServicePlan::class, function (Faker $faker) {
    return [
        'user_unique' => User::all()->random()->user_unique,
        'name_of_insurance_provider' => $faker->name,
        'office_location' => $faker->address,
        'asset_issued' => $faker->sentence,
        'sum_issued' => $faker->randomElement([2000000, 10000000, 40000000, 5000000, 60000000]),
        'insurance_cover' => $faker->name,
        'insurance_broker' => $faker->name,
        'name_of_reinsurance_service_provider' => $faker->name,
        'asset_reinsured' => $faker->name,
        'sum_reinsured' => $faker->randomElement([2000000, 10000000, 40000000, 5000000, 60000000]),
        'reason_for_omission' => $faker->sentence
    ];
});
