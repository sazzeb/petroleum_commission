<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\BankRegulation;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(BankRegulation::class, function (Faker $faker) {
    $next1 = [
        array(
            'list_of_service' => 'Financial Water breath',
            'expenditure_incured' => 32000,
        ),
        array(
            'list_of_service' => 'Commander service lover',
            'expenditure_incured' => 40000,
        ),
        array(
            'list_of_service' => 'all both one service',
            'expenditure_incured' => 20000
        )
    ];
    $erra1 = json_encode($next1);
    return [
        'user_unique' => User::all()->random()->user_unique,
        'name_of_bank' => $faker->name,
        'office_location' => $faker->address,
        'services_utilise_expenditure' => $erra1
    ];
});
