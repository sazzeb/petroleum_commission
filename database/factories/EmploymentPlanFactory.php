<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Models\EmploymmentPlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(EmploymmentPlan::class, function (Faker $faker) {
    return [
        'user_unique' => User::all()->random()->user_unique,
        'position' => $faker->name,
        'name' => $faker->name,
        'nationality' => $faker->randomElement(['ghana', 'user', 'ghana', 'germany', 'ghana', 'brazil']),
        'salary' => $faker->randomElement([12000, 305000, 45000, 50000, 4055600, 5000000]),
        'job_description' => $faker->sentence,
        'expertise' => $faker->sentence,
        'department' => $faker->randomElement(['technical', 'project manager', 'store keeper', 'accountant']),
        'gender' => $faker->randomElement(['male', 'female']),
        'rank_category' => $faker->randomElement(['management', 'core_staff', 'others']),
    ];
});
