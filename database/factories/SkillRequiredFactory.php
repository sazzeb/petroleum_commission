<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\SkillAcquired;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(SkillAcquired::class, function (Faker $faker) {
    return [
        'user_unique' => User::whereCategory('individual')->get()->random()->user_unique,
        'skill_category' => $faker->name,
        'skill_level' => $faker->randomElement([40, 50, 50, 20, 90]),
        'year_obtained' => $faker->year,
        'description' => $faker->sentence,
    ];
});
