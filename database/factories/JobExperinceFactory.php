<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\JobExperience;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(JobExperience::class, function (Faker $faker) {
    return [
        'user_unique' => User::whereCategory('individual')->get()->random()->user_unique,
        'organisation_name' => $faker->name,
        'job_title' => $faker->name,
        'industry' => $faker->name,
        'organisation_address' => $faker->address,
        'country_employmemnt' => $faker->country,
        'job_description' => $faker->sentence,
        'start_date' => Carbon::now(),
        'end_date' => Carbon::now()
    ];
});
