<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\CompanyDetail;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(CompanyDetail::class, function (Faker $faker) {
    $next = [
        array(
            'name' => 'prince walter',
            'percentage' => 40
        ),
        array(
            'name' => 'Walkter Benter',
            'percentage' => 20
        ),
        array(
            'name' => 'Moris Jonter',
            'percentage' => 5
        ),
        array(
            'name' => 'Grad bender',
            'percentage' => 25,
        )
    ];
    $next1 = [
        array(
            'name' => 'prince walter',
            'address' => 'No 4 chimbo street',
            'phone' => '0809367585',
            'percentage' => 40
        ),
        array(
            'name' => 'Walkter Benter',
            'address' => 'No 4 chimbo street',
            'phone' => '0809367585',
            'percentage' => 20
        ),
        array(
            'name' => 'Moris Jonter',
            'address' => 'No 4 chimbo street',
            'phone' => '0809367585',
            'percentage' => 5
        ),
        array(
            'name' => 'Grad bender',
            'address' => 'No 4 chimbo street',
            'phone' => '0809367585',
            'percentage' => 25,
        )
    ];
    $erra = json_encode($next);
    $erra4 = json_encode($next1);
    return [
        'user_unique' => User::all()->random()->user_unique,
        'company_name' => $faker->name,
        'company_registration_number' => $faker->randomElement(['RN080993', 'RN435743', 'RN65780993']),
        'tax_identity_number' => $faker->randomElement(['RN080993', 'RN435743', 'RN65780993']),
        'date_of_incorporation' => Carbon::now(),
        'date_of_commencement_of_service' => Carbon::now(),
        'petroleum_commision_permit_number' => $faker->randomElement(['RN080993', 'RN435743', 'RN65780993']),
        'ownership' => $faker->name,
        'service_provided' => $faker->name,
        'postal_address' => $faker->address,
        'telephone' => $faker->randomElement([2000000, 3000000, 4000000, 50000, 700000]),
        'website_address' => $faker->randomElement(['www.binta-oil.com', 'www.caronaOil.com', 'vinta.oil.com']),
        'house_or_plot_number' => $faker->randomElement(['No 120', 'No 12', 'No 50', 'No 1']),
        'names_of_shareholder_with_percentage' => $erra4,
        'street_name' => $faker->name,
        'city' => $faker->city,
        'region' => $faker->city,
        'head_email' => $faker->unique()->safeEmail,
        'name_of_head_of_company' => $faker->name,
        'head_position' => $faker->randomElement(['CEO', 'Manager', 'Director']),
        'head_phone' =>  $faker->unique()->safeEmail,
        'enquire_personal_name' => $faker->name,
        'enquire_personal_email' => $faker->unique()->safeEmail,
        'enquire_personal_position' => $faker->randomElement(['Manger', 'Treasurer', 'HR']),
        'enquire_personal_business_address' => $faker->address,
        'enquire_personal_address' => $faker->address

    ];
});
