<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\ResearchDevelopmentPlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(ResearchDevelopmentPlan::class, function (Faker $faker) {
    return [
        'user_unique' => User::all()->random()->user_unique,
        'title' => $faker->name,
        'content' => $faker->sentence,
        'start_date' => Carbon::now(),
        'end_date' => Carbon::now(),
    ];
});
