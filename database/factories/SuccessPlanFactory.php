<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\SuccessionPlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(SuccessionPlan::class, function (Faker $faker) {
    $next = [
        array(
            'name_of_ghanaian' => 'Peter Mansa',
            'ghanaian_working_experience' => 'carpenter',
            'ghanaian_position' => 'fourman',
            'ghanaian_certificate'=> 'bsc',
            'required_skill' => 'carpenter',
            'certicate_to_undertake' => 'mining personnel',
            'kpi' => '67%',
            'remarks' => 'i love the way he works' 
        ),
        array(
            'name_of_ghanaian' => 'mansa Mansa',
            'ghanaian_working_experience' => 'tailro',
            'ghanaian_position' => 'compainter',
            'ghanaian_certificate'=> 'bsc',
            'required_skill' => 'carpenter',
            'certicate_to_undertake' => 'mining personnel',
            'kpi' => '67%',
            'remarks' => 'i love the way he works' 
        ),
        array(
            'name_of_ghanaian' => 'jombo Mansa',
            'ghanaian_working_experience' => 'welder',
            'ghanaian_position' => 'printer',
            'ghanaian_certificate'=> 'bsc',
            'required_skill' => 'jobber',
            'certicate_to_undertake' => 'mining personnel',
            'kpi' => '67%',
            'remarks' => 'i love the way he works' 
        ),
    ];
    $erra = json_encode($next);

    return [
        'user_unique' => User::all()->random()->user_unique,
        'name_of_expatriate' => $faker->name,
        'expatriate_position' => $faker->name,
        'expatraite_certificate' => $faker->name,
        'expatraite_working_experince' => $faker->name,
        'remarks' => $faker->sentence,
        'start_date' => Carbon::now(),
        'end_date' => Carbon::now(),
        'ghanaians_detials' => $erra,
    ];
});
