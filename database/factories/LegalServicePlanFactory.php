<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Carbon\Carbon;
use App\User;
use App\Models\LegalServicePlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(LegalServicePlan::class, function (Faker $faker) {
    $next = [
        array(
            'name' => 'Properties managment',
        ),
        array(
            'name' => 'car insured combo',
        ),
        array(
            'migrate' => 'jambo jambo',
        )
    ];
    $erra = json_encode($next);


    return [
        'user_unique' => User::all()->random()->user_unique,
        'name_of_firm' => $faker->name,
        'date_of_engagement' => Carbon::now(),
        'location' => $faker->address,
        'past_six_month_obtained' => $erra,
        'next_six_month_obtained' => $erra,
        'expenditure_next_six_month' => $faker->randomElement([2000000, 3000000, 4000000, 50000, 700000]),
        'expenditure_last_six_month' => $faker->randomElement([2000000, 3000000, 4000000, 50000, 700000]),
        'nature_of_expenditure' => $faker->sentence,
        'external_solicitors' => $erra,
        'comprehensive_report' => $faker->sentence
    ];
});
