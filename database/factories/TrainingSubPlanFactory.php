<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\TrainingSubPlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TrainingSubPlan::class, function (Faker $faker) {
    return [
        'user_unique' => User::all()->random()->user_unique,
        'employee_name' => $faker->name,
        'position' => $faker->randomElement(['technology', 'ship reck', 'car mechnanics']),
        'type_of_training' => $faker->name,
        'detail_of_training' => $faker->sentence,
        'start_date' => Carbon::now(),
        'end_date' => Carbon::now(),
        'training_outcomes' => $faker->name,
        'cost' => $faker->randomElement([200000, 30000, 40000, 10000, 50000]),
        'remarks' => $faker->sentence,
    ];
});
