<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Carbon\Carbon;
use App\Models\AcademicRecord;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(AcademicRecord::class, function (Faker $faker) {
    return [
        'educational_level' => $faker->name,
        'user_unique' => User::whereCategory('individual')->get()->random()->user_unique,
        'country_obtained' => $faker->country,
        'discipline' => $faker->randomElement(['electrical', 'accounting', 'marketing']),
        'course_of_study' => $faker->randomElement(['electrical', 'accounting', 'marketing']),
        'start_year' => Carbon::now(),
        'end_year' => Carbon::now(),
        'name_of_institution' => $faker->name,
        'degree_obtained' => $faker->randomElement(['bsa', 'bsc', 'msc']),
    ];
});
