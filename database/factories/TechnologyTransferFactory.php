<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\User;
use App\Models\TechnologyTransferPlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(TechnologyTransferPlan::class, function (Faker $faker) {
    $next = [
        array(
            'name' => 'prince walter',
            'technology' => 'Technology',
            'carpenter' => 'sorter'
        ),
        array(
            'name' => 'Walkter Benter',
            'technology' => 'Technology',
            'carpenter' => 'sorter'
        ),
        array(
            'name' => 'Moris Jonter',
            'technology' => 'Technology',
            'carpenter' => 'sorter'
        )
    ];
    $erra = json_encode($next);
    return [
        'user_unique' => User::all()->random()->user_unique,
        'initiative_programme' => $faker->name,
        'start_date' => Carbon::now(),
        'end_date' => Carbon::now(),
        'implementaion_strategy' => $faker->sentence,
        'expenditure_incurred' => $faker->randomElement([2000000, 3000000, 4000000, 50000, 700000]),
        'jv_expertise_role' => $faker->name,
        'jv_ghanaian_role' => $erra,
        'jv_strategy_for_transfer_of_technology' => $faker->name,
        'jv_strategy_for_transfer_of_skill' => $faker->name
    ];
});
