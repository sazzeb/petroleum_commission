<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Carbon\Carbon;
use App\User;
use App\Models\FinancialServicePlan;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(FinancialServicePlan::class, function (Faker $faker) {
    $next1 = [
        array(
            'name' => 'Financial Water breath',
            'sum' => 32000,
        ),
        array(
            'name' => 'Commander service lover',
            'sum' => 40000,
        ),
        array(
            'name' => 'all both one service',
            'sum' => 20000
        )
    ];
    $next2 = [
        array(
            'name' => 'All For one servie',
            'sum' => 32000,
        ),
        array(
            'name' => 'One For all service',
            'sum' => 40000,
        ),
        array(
            'name' => 'That both in one',
            'sum' => 20000
        )
    ];
    $next3 = [
        array(
            'name' => 'All For one servie',
            'sum' => 32000,
        ),
        array(
            'name' => 'One For all service',
            'sum' => 40000,
        ),
        array(
            'name' => 'That both in one',
            'sum' => 20000
        )
    ];
    $erra1 = json_encode($next1);
    $erra2 = json_encode($next2);
    $erra3 = json_encode($next3);
    return [
        'user_unique' => User::all()->random()->user_unique,
        'name_of_banks' => $faker->name,
        'office_location' => $faker->address,
        'account_number' => $faker->randomElement([9994494, 2333436, 2737844, 2345648, 23485859]),
        'branch' => $faker->address,
        
        'date_of_engagement' => Carbon::now(),
        'financial_service_for_pass_six_month' => $erra1,
        'financial_service_for_next_six_month' => $erra2,
        'list_of_service_expenditure' => $erra3,
        'name_of_indigenous_bank' => $faker->name,
        'indigenous_bank_financial_expenditure' => $faker->randomElement([200000, 300000, 340000, 540000, 500000]),
        'indigenous_bank_date_of_engagement' => Carbon::now(),
    ];
});
