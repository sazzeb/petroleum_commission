<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\SuccessionPlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class SuccessionPlancontroller extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $id = auth()->user()->user_unique;
        $sucession = SuccessionPlan::whereUserUnique($id)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'sucession Records added',
                    'data' => $sucession
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function store()
    {
        $user = auth()->user()->user_unique;
        request()->validate([
            'ghanaians_detials' => 'required',
            'name_of_expatriate'=> 'required',
            'expatriate_position'=> 'required',
            'expatraite_certificate'=> 'required',
            'expatraite_working_experince'=> 'required',
            'remarks'=> 'required',
            'start_date'=> 'required|date',
            'end_date'=> 'required|date'
        ]);
        $data = $this->getData();
        try {
            if($dataT = SuccessionPlan::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'legal has been change',
                        'data' => $dataT,
                    ], 200);
                }
            }
            
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }

    public function update($id)
    {
        $user = auth()->user()->user_unique;
        request()->validate([
            'ghanaians_detials' => 'required',
            'name_of_expatriate'=> 'required',
            'expatriate_position'=> 'required',
            'expatraite_certificate'=> 'required',
            'expatraite_working_experince'=> 'required',
            'remarks'=> 'required',
            'start_date'=> 'required|date',
            'end_date'=> 'required|date'
        ]);
        $data = $this->getData();

        $succession = SuccessionPlan::whereUserUnique($user)->whereId($id)->first();
        try {
            if($succession->update($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'legal has been change',
                        'data' => $succession->fresh(),
                    ], 200);
                }
            }
            
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }

    public function show($id)
    {
        request()->validate([
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,txt,pdf,doc,docx|max:2048'
 
        ]);

        if(request()->hasFile('files')) {
            $auth = auth()->user()->user_unique;
            $company  = SuccessionPlan::whereUserUnique($auth)->whereId($id)->first();

            try {
                foreach(request()->file('files') as $file){
                    $name = $file->getClientOriginalName();
                    $dir = 'uploads/succession/details/';
                    $filename = uniqid().'_'.time().'_'.$name;
                    $move = $file->move($dir, $filename);
                    $filepath = 'uploads/succession/details/'.$filename;
                    if($move){
                        $company->image()->create([
                            'user_unique' => $auth,
                            'file' => $filepath
                        ]);
                    }
                    
                }
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! file has been added',
                        'data' => $company->image,
                    ], 201);
                }
            } catch (Exception $e) {
                return response()->json([
                            'error' => $e->getMessage
                        ], 403);
            }
        }
    }

    public function edit($id) {

    }

    public function destroy($id)
    {
        $user = auth()->user()->user_unique;

        $succession = SuccessionPlan::whereUserUnique($user)->whereId($id)->first();

        try {
            if (request()->expectsJson()){
                if($succession->delete()){
                    return response()->json(['message' => 'data deleted'], 200);
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    protected function getRule () {
        return [
            'name_of_expatriate' => 'required|string',
            'expatriate_position' => 'required|string',
            'expatraite_certificate' => 'required|string',
            'expatraite_working_experince' =>'required|string',
            'remarks' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'ghanaians_detials' => 'required'
        ];
    }

    protected function getData () {
        return [
            'user_unique' => auth()->user()->user_unique,
            'name_of_expatriate' => request()->name_of_expatriate,
            'ghanaians_detials' => request()->ghanaians_detials,
            'expatriate_position' => request()->expatriate_position,
            'expatraite_certificate' => request()->expatraite_certificate,
            'expatraite_working_experince' => request()->expatraite_working_experince,
            'remarks' => request()->remarks,
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
        ];
    }
}
