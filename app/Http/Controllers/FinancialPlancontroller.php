<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\FinancialServicePlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class FinancialPlancontroller extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $id = auth()->user()->user_unique;
        $financial = FinancialServicePlan::whereUserUnique($id)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Financial Records added',
                    'data' => $financial
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function store()
    {
        $user = auth()->user()->user_unique;
        $rules = $this->getRule();

        request()->validate([
            'name_of_banks' => 'required',
            'office_location' => 'required',
            'branch' => 'required',
            'date_of_engagement' => 'required|date',
            'financial_service_for_pass_six_month' => 'required',
            'financial_service_for_next_six_month' => 'required',
            'financial_expenditure_for_pass_six_month' => 'required',
            'financial_expenditure_for_next_six_month' => 'required',
            'list_of_service_expenditure' => 'required',
            'name_of_indigenous_bank' => 'required',
            'indigenous_bank_financial_expenditure' => 'required',
            'indigenous_bank_date_of_engagement' => 'required|date'
        ]);

        try {
            $data = $this->getData();

            $finance = FinancialServicePlan::whereUserUnique($user)->first();

            if ($finance) {
                if($finance->update($data)){
                    return response()->json([
                        'message' => 'Financial has been change',
                        'data' => $finance->fresh(),
                    ], 200);
                }
            } else {
                if($dataT = FinancialServicePlan::create($data)){
                    return response()->json([
                        'message' => 'Financial has been change',
                        'data' => $dataT,
                    ], 200);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => $e->getMessage()
            ]);
        }
    }

    public function update($id)
    {
        request()->validate([
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,txt,pdf,doc,docx|max:2048'
 
        ]);

        if(request()->hasFile('files')) {
            $auth = auth()->user()->user_unique;
            $company  = FinancialServicePlan::whereUserUnique($auth)->whereId($id)->first();

            try {
                foreach(request()->file('files') as $file){
                    $name = $file->getClientOriginalName();
                    $dir = 'uploads/finance/details/';
                    $filename = uniqid().'_'.time().'_'.$name;
                    $move = $file->move($dir, $filename);
                    $filepath = 'uploads/finance/details/'.$filename;
                    if($move){
                        $company->image()->create([
                            'user_unique' => $auth,
                            'file' => $filepath
                        ]);
                    }
                    
                }
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! file has been added',
                        'data' => $company->image,
                    ], 201);
                }
            } catch (Exception $e) {
                return response()->json([
                            'error' => $e->getMessage
                        ], 403);
            }
        }
    }

    public function show($id)
    {
        $user = auth()->user()->user_unique;

        $financial = FinancialServicePlan::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Financial record loaded',
                    'data' => $financial,
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function edit($id) {

    }

    public function destroy($id)
    {
        $user = auth()->user()->user_unique;

        $financial = FinancialServicePlan::whereUserUnique($user)->whereId($id)->first();

        try {
            if (request()->expectsJson()){
                if($financial->delete()){
                    return response()->json(['message' => 'data deleted'], 200);
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    protected function getRule() {
        return [
            'name_of_banks' => 'required',
            'office_location' => 'required',
            'branch' => 'required',
            'date_of_engagement' => 'required|date',
            'financial_service_for_pass_six_month' => 'required',
            'financial_service_for_next_six_month' => 'required',
            'financial_expenditure_for_pass_six_month' => 'required',
            'financial_expenditure_for_next_six_month' => 'required',
            'list_of_service_expenditure' => 'required',
            'name_of_indigenous_bank' => 'required',
            'indigenous_bank_financial_expenditure' => 'required',
            'indigenous_bank_date_of_engagement' => 'required|date'
        ];
    }

    protected function getData() {
        
        return [
            'user_unique' => auth()->user()->user_unique,
            'name_of_banks' => request()->name_of_banks,
            'office_location' => request()->office_location,
            'branch' => request()->branch,
            'date_of_engagement' => request()->date_of_engagement,
            'financial_service_for_pass_six_month' => request()->financial_service_for_pass_six_month,
            'financial_service_for_next_six_month' => request()->financial_service_for_next_six_month,
            'financial_expenditure_for_pass_six_month' => request()->financial_expenditure_for_pass_six_month,
            'financial_expenditure_for_next_six_month' => request()->financial_expenditure_for_next_six_month,
            'list_of_service_expenditure' => request()->list_of_service_expenditure,
            'name_of_indigenous_bank' => request()->name_of_indigenous_bank,
            'indigenous_bank_financial_expenditure' => request()->indigenous_bank_financial_expenditure,
            'indigenous_bank_date_of_engagement' => request()->indigenous_bank_date_of_engagement
        ];
    }
}
