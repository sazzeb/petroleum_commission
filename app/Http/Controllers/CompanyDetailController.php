<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use App\Models\FileUpload;
use App\Models\CompanyDetail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class CompanyDetailController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $id = auth()->user()->user_unique;
        $company = CompanyDetail::whereUserUnique($id)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Academic Records added',
                    'data' => $company
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function store()
    {
        $user = auth()->user()->user_unique; 
        
        request()->validate($this->getRule());

        try {
            $user = CompanyDetail::whereUserUnique(auth()->user()->user_unique)->first();
            $data = $this->getData();

            if($user){
                if($user->update($data)){
                    if(request()->expectsJson()){
                        return response()->json([
                            'message' => 'value has been updated',
                            'data' => $user->fresh(),
                        ], 200);
                    }
                }
            }else {
                if($dataT = CompanyDetail::create($data)){
                    if(request()->expectsJson()){
                        return response()->json([
                            'message' => 'Company Info has been change',
                            'data' => $dataT,
                        ], 200);
                    } else {
                        return redirect()->with()->with('success', 'Congrattulation data has been added sucessfully');
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()]);
        }
    }

    public function update($id)
    {

        request()->validate([
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,txt,pdf,doc,docx|max:2048'
 
        ]);

        if(request()->hasFile('files')) {
            $auth = auth()->user()->user_unique;
            $company  = CompanyDetail::whereUserUnique($auth)->whereId($id)->first();

            try {
                foreach(request()->file('files') as $file){
                    $name = $file->getClientOriginalName();
                    $dir = 'uploads/company/details/';
                    $filename = uniqid().'_'.time().'_'.$name;
                    $file->move($dir, $filename);
                    $filepath = 'uploads/company/details/'.$filename;
                    $company->image()->create([
                        'user_unique' => $auth,
                        'file' => $filepath
                    ]);
                }
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! file has been added',
                        'data' => $company->image,
                    ], 201);
                }
            } catch (Exception $e) {
                return response()->json([
                            'error' => $e->getMessage
                        ], 403);
            }
        }

    }

    public function show($id)
    {
        $user = auth()->user()->user_unique;

        $company = CompanyDetail::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Job experience Loaded loadeed',
                    'data' => $company,
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function edit($id) {

    }

    public function destroy($id)
    {
        $user = auth()->user()->user_unique;

        $company = CompanyDetail::whereUserUnique($user)->whereId($id)->first();

        try {
            if (request()->expectsJson()){
                if($company->delete()){
                    return response()->json(['message' => 'data deleted'], 200);
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    protected function getRule () {
       return  [
            'company_name' => 'required|string',
            'company_registration_number' => 'required',
            'tax_identity_number' => 'required',
            'date_of_incorporation' => 'required|date',
            'date_of_commencement_of_service' => 'required|date',
            'petroleum_commision_permit_number' => 'required',
            'ownership' => 'required',
            'service_provided' => 'required',
            'postal_address' => 'required',
            'telephone' => 'required|integer',
            'website_address' => 'required',
            'house_or_plot_number' => 'required',
            'names_of_shareholder_with_percentage' => 'required',
            'street_name' => 'required',
            'city' => 'required',
            'region' => 'required',
            'head_email' => 'required',
            'name_of_head_of_company' => 'required',
            'head_position' => 'required',
            'head_phone' =>  'required|integer',
            'enquire_personal_name' => 'required',
            'enquire_personal_email' => 'required|email',
            'enquire_personal_position' => 'required',
            'enquire_personal_business_address' => 'required',
            'enquire_personal_address' => 'required'];
    }
    

    protected function getData() {
        

        return [
            'user_unique' => auth()->user()->user_unique,
            'company_name' => request()->company_name,
            'company_registration_number' => request()->company_registration_number,
            'tax_identity_number' => request()->tax_identity_number,
            'date_of_incorporation' => request()->data_of_incorporation,
            'date_of_commencement_of_service' => request()->date_of_commencement_of_service,
            'petroleum_commision_permit_number' => request()->petroleum_commision_permit_number,
            'ownership' => request()->ownership,
            'service_provided' => request()->service_provided,
            'postal_address' => request()->postal_address,
            'telephone' => request()->telephone,
            'website_address' => request()->website_address,
            'house_or_plot_number' => request()->house_or_plot_number, 
            'names_of_shareholder_with_percentage' => request()->names_of_shareholder_with_percentage,
            'street_name' => request()->street_name,
            'city' => request()->city,
            'region' => request()->region,
            'head_email' => request()->head_email,
            'name_of_head_of_company' => request()->name_of_head_of_company,
            'head_position' => request()->head_position,
            'head_phone' =>  request()->head_phone,
            'enquire_personal_name' => request()->enquire_personal_name,
            'enquire_personal_email' => request()->enquire_personal_email,
            'enquire_personal_position' => request()->enquire_personal_position,
            'enquire_personal_business_address' => request()->enquire_personal_business_address,
            'enquire_personal_address' => request()->enquire_personal_address,
    
        ];
    }
}
