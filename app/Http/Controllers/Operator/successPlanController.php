<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SuccessionPlan;


class SuccessPlanController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user()->user_unique;

        $success = SuccessionPlan::whereUserUnique($user)->with('image')->latest()->get();
        return view('operator.success-Plan', ['successions' => $success]);  
    }
}
