<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CompanyDetail;

class CompanyDetailController extends Controller
{

    public function __construct(){
        $this->middleware('auth');;
    }
    
    public function index()
    {
        $user = auth()->user()->user_unique;
        $company_detail = CompanyDetail::whereUserUnique($user)->first();
        return view('operator.companyDetails', ['company_detail' => $company_detail, 'uploads' => $company_detail ? $company_detail->image : []]);  
    }
}
