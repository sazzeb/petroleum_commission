<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\IndigenousBankPlan;


class IndigenousPlanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }


    public function index(){
        $user = auth()->user()->user_unique;
        $plans = IndigenousBankPlan::whereUserUnique($user)->latest()->get();
        return view('operator.indigenous-plan', ['plans' => $plans]);
    }
}
