<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TechnologyTransferPlan;

class TechnologyTransferProgramController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){

        $user = auth()->user()->user_unique;
        $tech = TechnologyTransferPlan::whereUserUnique($user)->with('image')->latest()->get();

        return view('operator.technology-Program', ['techs' => $tech]);
    }
}
