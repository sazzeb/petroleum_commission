<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InsuranceServicePlan;


class InsuranceServicePlanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $user = auth()->user()->user_unique;
        $insures = InsuranceServicePlan::whereUserUnique($user)->with('image')->latest()->get();

        return view('operator.insurance-Plan', ['insures' => $insures]);
    }
}
