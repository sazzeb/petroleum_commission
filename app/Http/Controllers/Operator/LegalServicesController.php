<?php

namespace App\Http\Controllers\Operator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LegalServicePlan;

class LegalServicesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $user = auth()->user()->user_unique;
        $legals = LegalServicePlan::whereUserUnique($user)->first();
        return view('operator.legal-Services', ['legals' => $legals, 'uploads' => $legals ? $legals->image : [] ]);
    }
}
