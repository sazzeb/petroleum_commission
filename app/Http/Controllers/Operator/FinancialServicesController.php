<?php

namespace App\Http\Controllers\Operator;


use App\Models\FinancialServicePlan;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FinancialServicesController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
        $user = auth()->user()->user_unique;
        $financial = FinancialServicePlan::whereUserUnique($user)->first();
        return view('operator.financial-Services', ['financial_detail' => $financial, 'uploads' => $financial ? $financial->image : []]);
    }
}
