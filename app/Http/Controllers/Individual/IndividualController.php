<?php

namespace App\Http\Controllers\Individual;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndividualController extends Controller
{
    //
    public function dashboard(){
        return view('individual.dashboard');
    }
}
