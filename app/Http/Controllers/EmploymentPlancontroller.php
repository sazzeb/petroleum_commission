<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\EmploymmentPlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class EmploymentPlancontroller extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $id = auth()->user()->user_unique;
        $employments = EmploymmentPlan::whereUserUnique($id)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Academic Records Loaded loadeed',
                    'data' => $employments
                ]);
            }else{
                return view('operator.employee.index',compact('employments'));
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }
    public function store()
    {
        $user = auth()->user()->user_unique;
        $rules = $this->getRule();

        try {
            $this->validate(request(), $rules);
            $data = $this->getData();
            if($dataT = EmploymmentPlan::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => '   data has been changed',
                        'data' => $dataT,
                    ], 200);
                }else{
                    return redirect()->back()->with('success',"Employee added successfully");
                }
            }

        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function update($id)
    {
//        dd(request()->all());
        $user = auth()->user()->user_unique;
        $rules = $this->getRule();

        try {
            $this->validate(request(), $rules);

            $data = $this->getData();
            $employment = EmploymmentPlan::whereUserUnique($user)->whereId($id)->first();

            if($employment->update($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'skill data has been changed',
                        'data' => $employment->fresh(),
                    ], 200);
                }
                else{
                    return redirect()->back()->with('success','Update is successful');
                }
            }

        } catch (ValidationException $e) {
            return redirect()->back()->with('errors', $e->errors());
        }
    }

    public function show($id)
    {
        $user = auth()->user()->user_unique;

        $employment = EmploymmentPlan::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Job experience Loaded loadeed',
                    'data' => $employment,
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function edit($id) {
        $user = auth()->user()->user_unique;
        $details = [];
        $employment = EmploymmentPlan::whereUserUnique($user)->get();

        $details = $employment;
        $details['no_ghanaians_male_managemnent'] = $this->getGhanaiansMaleManagement();
        $details['no_ghanaians_female_managemnent'] = $this->getGhanaiansFemaleManagement();
        $details['no_expatraite_male_managemnent'] = $this->getExpatraitMaleManagement();
        $details['no_expatraite_female_managemnent'] = $this->getExpatraitFemaleManagement();

        $details['no_ghanaians_male_core_staff'] = $this->getGhanaiansMaleCore();
        $details['no_ghanaians_female_core_staff'] = $this->getGhanaiansFemaleCore();
        $details['no_expatraite_male_core_staff'] = $this->getExpatraitMaleCore();
        $details['no_expatriate_female_core_staff'] = $this->getExpatraitFemaleCore();

        $details['no_ghanaians_male'] = $this->getGhanaiansMale();
        $details['no_ghanaians_female'] = $this->getGhanaiansMale();
        $details['no_expatraite_male'] = $this->getGhanaiansMale();
        $details['no_expatraite_female'] = $this->getGhanaiansMale();

        $details['expenditure_ghanaians'] = $this->getGhanaiansExpenditure();
        $details['expenditure_expatraite'] = $this->getExpatraitExpenditure();

        $details['expenditure_total'] = $this->getGhanaiansExpenditure() + $this->getExpatraitExpenditure();
        try {

            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Job experience Loaded loadeed',
                    'data' => $details,
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $user = auth()->user()->user_unique;

        $employment = EmploymmentPlan::whereUserUnique($user)->whereId($id)->first();

        try {
            if($employment->delete()){
                if (request()->expectsJson()){
                    return response()->json(['message' => 'data deleted'], 200);
                }
                return redirect()->back()->with('success','Deleted Successfully');

            }

        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }


    public function getGhanaiansMaleManagement () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('management')
                ->whereGender('male')->whereNationality('ghana')
                ->get()->count();
        return $male;
    }

    public function getGhanaiansFemaleManagement () {
        $user = auth()->user()->user_unique;

        $female = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('management')
                ->whereGender('female')->whereNationality('ghana')
                ->get()->count();
        return $female;
    }

    public function getExpatraitMaleManagement () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('management')
                ->whereGender('male')->whereNotIn('nationality', ['ghana'])
                ->get()->count();
        return $male;
    }

    public function getExpatraitFemaleManagement () {
        $user = auth()->user()->user_unique;

        $female = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('management')
                ->whereGender('female')->whereNotIn('nationality', ['ghana'])
                ->get()->count();
        return $female;
    }

    public function getGhanaiansMaleCore () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('core_staff')
                ->whereGender('male')->whereNationality('ghana')
                ->get()->count();
        return $male;
    }

    public function getGhanaiansFemaleCore () {
        $user = auth()->user()->user_unique;

        $female = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('core_staff')
                ->whereGender('female')->whereNationality('ghana')
                ->get()->count();
        return $female;
    }

    public function getExpatraitMaleCore () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('core_staff')
                ->whereGender('male')->whereNotIn('nationality', ['ghana'])
                ->get()->count();
        return $male;
    }

    public function getExpatraitFemaleCore () {
        $user = auth()->user()->user_unique;

        $female = EmploymmentPlan::whereUserUnique($user)
                ->whereRankCategory('core_staff')
                ->whereGender('female')->whereNotIn('nationality', ['ghana'])
                ->get()->count();
        return $female;
    }


    public function getGhanaiansMale () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereGender('male')->whereNationality('ghana')->get()->count();
        return $male;
    }

    public function getGhanaiansFemale () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereGender('female')->whereNationality('ghana')->get()->count();
        return $male;
    }

    public function getExpatraitMale () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereGender('male')->whereNotIn('nationality', ['ghana'])->get()->count();
        return $male;
    }

    public function getExpatraitFemale () {
        $user = auth()->user()->user_unique;

        $male = EmploymmentPlan::whereUserUnique($user)
                ->whereGender('female')->whereNotIn('nationality', ['ghana'])->get()->count();
        return $male;
    }

    public function getGhanaiansExpenditure () {
        $user = auth()->user()->user_unique;

        $ghana = EmploymmentPlan::whereUserUnique($user)->whereNotIn('nationality', ['ghana'])->get()->sum('salary');
        return $ghana;
    }

    public function getExpatraitExpenditure () {
        
        $user = auth()->user()->user_unique;
        $expatraite = EmploymmentPlan::whereUserUnique($user)->whereNotIn('nationality', ['ghana'])->get()->sum('salary');
        return $expatraite;
    }

    protected function getRule () {
        return [
            'position' => 'required|string',
            'name' => 'required|string',
            'nationality' => 'required|string',
            'job_description' => 'required|string',
            'expertise' => 'required|string',
            'department' => 'required|string',
            'gender' => 'required|string',
            'salary' => 'required|integer',
            'rank_category' => 'required|string',
        ];
    }
    

    protected function getData() {
        return [
            'user_unique' => auth()->user()->user_unique,
            'position' => request()->position,
            'name' => request()->name,
            'salary' => request()->salary,
            'nationality' => request()->nationality,
            'job_description' => request()->job_description,
            'expertise' => request()->expertise,
            'department' => request()->department,
            'gender' => request()->gender,
            'rank_category' => request()->rank_category,
        ];
    }
}
