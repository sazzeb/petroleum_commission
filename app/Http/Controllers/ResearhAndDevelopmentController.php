<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\ResearchDevelopmentPlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class ResearhAndDevelopmentController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    
    public function index()
    {
        
    }

    
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
        $user = auth()->user()->user_unique;

        request()->validate([
            'title' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'content' => 'required|string',
        ]);

        try {
            $data = $this->getData();
            if($newData = ResearchDevelopmentPlan::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'data' => $newData,
                    ], 201);
                }
            }
        } catch (Exception $e){
            return response()->json(['errors' => $e->getMessage()], 403);
        }
    }

    
    public function show($id)
    {
        request()->validate([
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,txt,pdf,doc,docx|max:2048'
 
        ]);

        if(request()->hasFile('files')) {
            $auth = auth()->user()->user_unique;
            $company  = ResearchDevelopmentPlan::whereUserUnique($auth)->whereId($id)->first();

            try {
                foreach(request()->file('files') as $file){
                    $name = $file->getClientOriginalName();
                    $dir = 'uploads/research/details/';
                    $filename = uniqid().'_'.time().'_'.$name;
                    $move = $file->move($dir, $filename);
                    $filepath = 'uploads/research/details/'.$filename;
                    if($move){
                        $company->image()->create([
                            'user_unique' => $auth,
                            'file' => $filepath
                        ]);
                    }
                    
                }
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! file has been added',
                        'data' => $company->image,
                    ], 201);
                }
            } catch (Exception $e) {
                return response()->json([
                            'error' => $e->getMessage
                        ], 403);
            }
        }
    }

    
    public function edit($id)
    {
        
    }

    
    public function update($id)
    {
        $user = auth()->user()->user_unique;

        request()->validate([
            'title' => 'required|string',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'content' => 'required|string',
        ]);

        $insured = ResearchDevelopmentPlan::whereUserUnique($user)->whereId($id)->first();

        try {
            $data = $this->getData();

            if($insured->update($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'data' => $insured->fresh(),
                    ], 201);
                }
            }
        } catch (Exception $e){
            return response()->json(['errors' => $e->getMessage()], 403);
        }
    }

    
    public function destroy($id)
    {
        
    }

    protected function getData (){
        return [
            'user_unique' => auth()->user()->user_unique,
            'title' => request()->title,
            'content' => request()->content,
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
 
        ];
    }
}
