<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Models\LegalServicePlan;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LegalPlancontroller extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $id = auth()->user()->user_unique;
        $legal = LegalServicePlan::whereUserUnique($id)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'legal Records added',
                    'data' => $legal
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function store()
    {
        $user = auth()->user()->user_unique;
        $rules = $this->getRule();

        request()->validate([
            'name_of_firm' => 'required|string',
            'date_of_engagement' => 'required|date',
            'location' => 'required|string',
            'past_six_month_obtained' => 'required',
            'next_six_month_obtained' => 'required',
            'expenditure_next_six_month' => 'required',
            'expenditure_last_six_month' => 'required',
            'nature_of_expenditure' => 'required',
            'comprehensive_report' => 'required'
        ]);


        try {
            $finance = LegalServicePlan::whereUserUnique($user)->first();

            $data = $this->getData();

            if ($finance) {
                if($finance->update($data)){
                    return response()->json([
                        'message' => 'Legal has been change',
                        'data' => $finance->fresh(),
                    ], 200);
                }
            } else {
                if($dataT = LegalServicePlan::create($data)){
                    return response()->json([
                        'message' => 'Legal has been change',
                        'data' => $dataT,
                    ], 200);
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                'errors' => $e->getMessage()
            ]);
        }
    }

    public function update($id)
    {
        request()->validate([
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,txt,pdf,doc,docx|max:2048'
 
        ]);

        if(request()->hasFile('files')) {
            $auth = auth()->user()->user_unique;
            $company  = LegalServicePlan::whereUserUnique($auth)->whereId($id)->first();

            try {
                foreach(request()->file('files') as $file){
                    $name = $file->getClientOriginalName();
                    $dir = 'uploads/legal/details/';
                    $filename = uniqid().'_'.time().'_'.$name;
                    $move = $file->move($dir, $filename);
                    $filepath = 'uploads/legal/details/'.$filename;
                    if($move){
                        $company->image()->create([
                            'user_unique' => $auth,
                            'file' => $filepath
                        ]);
                    }
                    
                }
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! file has been added',
                        'data' => $company->image,
                    ], 201);
                }
            } catch (Exception $e) {
                return response()->json([
                            'error' => $e->getMessage
                        ], 403);
            }
        }
    }

    public function show($id)
    {
        $user = auth()->user()->user_unique;

        $legal = LegalServicePlan::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'legal record loaded',
                    'data' => $legal,
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    public function edit($id) {

    }

    public function destroy($id)
    {
        $user = auth()->user()->user_unique;

        $legal = LegalServicePlan::whereUserUnique($user)->whereId($id)->first();

        try {
            if (request()->expectsJson()){
                if($legal->delete()){
                    return response()->json(['message' => 'data deleted'], 200);
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('erros', $e->getMessage());
        }
    }

    protected function getRule () {
    
        return [
            'name_of_firm' => 'required|string',
            'date_of_engagement' => 'required|date',
            'location' => 'required|string',
            'past_six_month_obtained' => 'required',
            'next_six_month_obtained' => 'required',
            'expenditure_next_six_month' => 'required|integer',
            'expenditure_last_six_month' => 'required|integer',
            'nature_of_expenditure' => 'required',
            'comprehensive_report' => 'required'
        ];
    }

    protected function getData () {

        return [
            'user_unique' => auth()->user()->user_unique,
            'name_of_firm' => request()->name_of_firm,
            'date_of_engagement' => request()->date_of_engagement,
            'location' => request()->location,
            'past_six_month_obtained' => request()->past_six_month_obtained,
            'next_six_month_obtained' => request()->next_six_month_obtained,
            'expenditure_next_six_month' => request()->expenditure_next_six_month,
            'expenditure_last_six_month' => request()->expenditure_last_six_month,
            'nature_of_expenditure' => request()->nature_of_expenditure,
            // 'external_solicitors' => request()->external_solicitors_expenditure,
            'comprehensive_report' => request()->comprehensive_report,
        ];
    }
}
