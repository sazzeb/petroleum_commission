<?php

namespace App\Http\Controllers\Api\Auth;

use App\User;
use App\Http\Controllers\Controller;
use function dd;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Redis;
use Illuminate\Validation\ValidationException;
use function request;

class RegistrationController extends Controller
{

    public function login()
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // return request()->all();

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();
            // $success['token'] =  $user->createToken('access_token')-> accessToken;
            
            if($user->category == "individual"){
                return $this->loginUserIn($user,$user->category);
            }
            if($user->category == "service_company"){
                return $this->loginUserIn($user,$user->category);
            }
            if($user->category == "admin"){
                return $this->loginUserIn($user,$user->category);
            }
            if($user->category == "operators"){
                return $this->loginUserIn($user,$user->category);
            }

            return response()->json([
                'success' => 'congrate you have logged in',
                $user,
            ], 200); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 

    }

    public function loginApi()
    {
        try {
            request()->validate([
                'email' => 'required|email',
                'password' => 'required',
            ]);
        } catch (ValidationException $e) {
            return response()->json([
                'error' => $e->errors(),
                'message' => $e->getMessage()
            ], 422);
        }

        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();
            $success['token'] =  $user->createToken('access_token')-> accessToken; 
            return response()->json(['success' => $success], 200); 
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    // this method goes to company for signup
    public function create()
    {
        $rules = $this->getCompany();
        try {
            $this->validate(request(), $rules);
        } catch (ValidationException $e) {
            return response()->json([
                'error' => $e->errors(),
                'message' => $e->getMessage()
            ], 422);
        }

        try {
            $data = $this->getCompanyData(request()->all());
            if($individual = User::create($data)){
                return response()->json([
                    'message' => 'Congratulation you have sign up',
                    'data' => $individual,
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 400);
        }
    }

    // this method goes to individual for signup

    
    public function store()
    {

        $rules = $this->getRule();
        try {
            $this->validate(request(), $rules);
        } catch (ValidationException $e) {
            return redirect()->back()->with("errors",$e->errors());
//            return response()->json([
//                'error' => $e->errors(),
//                'message' => $e->getMessage()
//            ], 422);
        }

        try {
            $data = $this->getData();
            if($individual = User::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation you have sign up',
                        'data' => $individual,
                    ], 200);
                }else{
                    if($individual->category == "individual"){
                        return $this->loginUserIn($individual,$individual->category);
                    }
                    if($individual->category == "service_company"){
                        return $this->loginUserIn($individual,$individual->category);
                    }
                    if($individual->category == "admin"){
                        return $this->loginUserIn($individual,$individual->category);
                    }
                    if($individual->category == "operators"){
                        return $this->loginUserIn($individual,$individual->category);
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 400);
        }
    }
    protected function loginUserIn($user,$type){
        Auth::login($user);
        if($type === "operators"){
            
            return Redirect::to('/operator/dashboard');
        }
        if($type === "individual"){
            return Redirect::to('/individual/dashboard');
        }
        if($type === "service_company"){
            return Redirect::to('/service-company/dashboard');
        }
        if($type === "admin"){
            return Redirect::to('/admin/dashboard');
        }
        
    }
    protected function getRule(){
        return [
            'name' => 'required|string|min:3|max:190',
            'email' => 'required|max:120|min:6|email|unique:users',
            'password' => 'required|min:3|max:60',
            'confirm_password' => 'same:password',
            'gender' => 'required|string',
            'phone' => 'required',
        ];
    }

    protected function getCompany(){
        return [
            'name' => 'required|string|min:3|max:190',
            'email' => 'required|max:120|min:6|email|unique:users',
            'password' => 'required|min:3|max:60',
            'confirm_password' => 'same:password',
            'registration_number' => 'required',
        ];
    }

    protected function getData() {
        $password = Hash::make(request()->password);
        return [
            'name' => request()->name,
            'email' => request()->email,
            'gender' => request()->gender,
            'country' => "Ghana",
            'phone' => request()->phone,
            'tax_identification_number' => request()->tax_identification_number,
            'registration_number' => request()->registration_number,
            'category' => request()->category,
            'password' => $password
        ];
    }

    public function logout () {
        auth()->logout();
        return redirect('/login');
    }
}
