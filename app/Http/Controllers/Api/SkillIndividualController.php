<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\SkillAcquired;
use App\Models\JobExperience;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;


class SkillIndividualController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $user = auth()->user()->user_unique;

        $job = JobExperience::whereUserUnique($user)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Job Records Loaded loadeed',
                    'data' => $job
                ]);
            }else{
                return view('individual.job-Experience', compact("job"));
            }
        } catch (\Exception $e) {

            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function all_skill()
    {
        $user = auth()->user()->user_unique;

        $skill = SkillAcquired::whereUserUnique($user)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Academic Records Loaded loadeed',
                    'data' => $skill,
                ]);
            }else{
                return view('individual.skills-Record', compact("skill"));
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function show($id)
    {
        $user = auth()->user()->user_unique;

        $job = JobExperience::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Academic Records Loaded loadeed',
                    'data' => $job
                ]);
            }
        } catch (\Exception $e) {

            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function one_skill($id)
    {
        $user = auth()->user()->user_unique;

        $skill = SkillAcquired::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Job experience Loaded loadeed',
                    'data' => $skill,
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function store()
    {
        // dd(request()->all());

        $rules = $this->getJobExperienceRule();
        request()->validate($rules);
        $data = $this->getJobExperienceData();

        try {
            if($dataT = JobExperience::create($data)){
                 if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! you have added an job experience',
                        'data' => $dataT,
                    ], 200);
                }else{
                    return redirect()->back()->with('success', 'Job experience record created');
                }
            } else {
                return redirect()->back()->with('warning', 'Job experience record not created');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function add_skill()
    {
        // dd(request()->all());

        $rules = $this->getSkillAcquireRule();
        request()->validate($rules);
        $data = $this->getSkillAcquiredData();

        try {

            if($dataT = SkillAcquired::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! you have added new skill',
                        'data' => $dataT,
                    ], 200);
                }else{
                    return redirect()->back()->with('success', 'Skill acquired record created');
                }
            } else {
                return redirect()->back()->with('warning', 'Skill acquired record not created');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function update($id)
    {
        $user = auth()->user()->user_unique;
        $rules = $this->getJobExperienceRule();
        request()->validate($rules);
        $data = $this->getJobExperienceData();
        $job = JobExperience::whereUserUnique($user)->whereId($id)->first();

        try {
            
            if($job->update($data)){
               
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Job experience has beeen updated',
                        'data' => $job->fresh(),
                    ], 200);
                }else {
                    return redirect()->back()->with('success', 'Job experience record  updated');
                }
                
            } else {
                return redirect()->back()->with('warning', 'Job experience record not updated');
            }
            
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function update_skill($id)
    {
        $user = auth()->user()->user_unique;
        $rules = $this->getSkillAcquireRule();
        $this->validate(request(), $rules);
        $data = $this->getSkillAcquiredData();
        $skill = SkillAcquired::whereUserUnique($user)->whereId($id)->first();
        

        try {
            

           
            if( $skill->update($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'skill data has been changed',
                        'data' => $dataT,
                    ], 200);
                }else {
                    return redirect()->back()->with('success', 'Job experience record  updated');
                }
                
            } else {
                return redirect()->back()->with('warning', 'Job experience record not updated');
            
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $user = auth()->user()->user_unique;
        $job = JobExperience::whereUserUnique(auth()->user()->user_unique)->whereId($id)->first();
        try {
            if( $job->delete()){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Record has been deleted'
                    ], 200);
                }else {
                    return redirect()->back()->with('success', 'Record has been deleted');
                }
                
            } else {
                return redirect()->back()->with('warning', 'Record has been deleted');
            
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function remove_skill($id)
    {
        $job = SkillAcquired::whereUserUnique(auth()->user()->user_unique)->whereId($id)->first();
        
        try {
            if( $job->delete()){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Record has been deleted'
                    ], 200);
                }else {
                    return redirect()->back()->with('success', 'Record has been deleted');
                }
                
            } else {
                return redirect()->back()->with('warning', 'Record has been deleted');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    protected function getJobExperienceRule () {
        return [
            'organisation_name' => 'required|string',
            'job_title' => 'required|string',
            'industry' => 'required|string',
            'organisation_address' => 'required|string',
            'country_employmemnt' => 'required|string',
            'job_description' =>'required|string',
            'start_date' => 'required|date',
            'end_date' =>'required|date',
        ];
    }

    protected function getJobExperienceData () {

        if(request()->file('files')){
            $validation = Validator::make(request()->all(), [
                'files' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
            ]);
    
            if ($validation->fails()){
                return ['fail' => true, 'errors' => $validation->errors()];
            }
    
        }
        if(request()->file('files')){
            $name = request()->file('files');
            $file = $name->getClientOriginalName();
            $filename = uniqid().'_'.time().'_'.$name;
            $filepath = 'uploads/individual/job_experince/'.$filename;
        }
        
        $file = request()->file('files') ? $filepath : null;

        return [
            'user_unique' => auth()->user()->user_unique,
            'organisation_name' => request()->organisation_name,
            'job_title' => request()->job_title,
            'industry' => request()->industry,
            'organisation_address' => request()->organisation_address,
            'country_employmemnt' => request()->country_employmemnt,
            'job_description' => request()->job_description,
            'start_date' => request()->start_date,
            'end_date' => request()->end_date,
            'files' => $file
        ];
    }

    protected function getSkillAcquireRule () {
        return [
            'skill_category' => 'required|string',
            'skill_level' => 'required|integer',
            'year_obtained' => 'required|string',
            'description' => 'required|string',
        ];
    }

    protected function getSkillAcquiredData () {
        
        return [
            'user_unique' => auth()->user()->user_unique,
            'skill_category' => request()->skill_category,
            'skill_level' => request()->skill_level,
            'year_obtained' => request()->year_obtained,
            'description' => request()->description,
        ];
    }
}
