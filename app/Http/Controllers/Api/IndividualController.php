<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Models\AcademicRecord;
use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Session;

class IndividualController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function index () {
        $user =  auth()->user();

        try {
            if (request()->expectsJson()){
                return response()->json([
                    'message' => 'User loadeed',
                    'data' => $user
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function update_user() {
        $auth =  auth()->user();

        $user = User::whereUserUnique($auth->user_unique)->whereCategory('individual')->first();


        $data = $this->getUserData();

        
        try {
            if(request()->expectsJson()){
                if ($userNew = $user->update($data)) {
                    return response()->json([
                        'message' => 'User loadeed',
                        'data' => $user
                    ], 201);
                } else {
                    return response()->json([
                        'message' => 'user information could not be updated!',
                    ], 201);
                }
            }
            
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }

    }

    protected function getUserData () {
        if(request()->file('files')){
            request()->validate(['files' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048']);
    
        }
        $name = request()->file('files')->getClientOriginalName();
        $filename = uniqid().'_'.time().'_'.$name;
        $filepath = 'uploads/individual/profile/'.$filename;
        $file = request()->file('files') ? $filepath : null;
        return [
            'name' => request()->name,
            'gender' => request()->gender,
            'country' => request()->country,
            'phone' => request()->phone,
            'category' => request()->category,
            'files' => $file,
        ];
    } 

    public function store () {
        // dd(request()->all());
        
        $rules = $this->getAcademicRule();
        request()->validate($rules);
        $data = $this->getAcademicData();

        try {
            if($dataT = AcademicRecord::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! you have added an academic record',
                        'data' => $dataT,
                    ], 200);
                }else{
                    return redirect()->back()->with('success', 'Academic record created');
                }
            } else {
                return redirect()->back()->with('warning', 'Academic record not created');
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function update ($id) {
        $user = auth()->user()->user_unique;
        $rules = $this->getAcademicRule();
        request()->validate($rules);
        $data = $this->getAcademicData();
        $academic = AcademicRecord::whereUserUnique($user)->whereId($id)->first();

        try {
            if($academic->update($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! you have updated an academic record',
                        'data' => $academic->fresh(),
                    ], 200);
                } else {
                    return redirect()->back()->with('success', 'Academic record  updated');
                }
                
            } else {
                return redirect()->back()->with('warning', 'Academic record not updated');
            
            }
            
        } catch (\Exception $e) {

            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function show()
    {
        $user = auth()->user()->user_unique;

        $academic = AcademicRecord::whereUserUnique($user)->get();

        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Academic Records Loaded loadeed',
                    'data' => $academic
                ]);
            }else{
                return view('individual.academic-Record', compact("academic"));
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }

    }

    public function edit($id)
    {
        $user = auth()->user()->user_unique;

        $academic = AcademicRecord::whereUserUnique($user)->whereId($id)->first();
        try {
            if(request()->expectsJson()){
                return response()->json([
                    'message' => 'Academic Records Loaded loadeed',
                    'data' => $academic
                ]);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    public function destroy ($id) {
        $user = auth()->user()->user_unique;
        $academic = AcademicRecord::whereUserUnique(auth()->user()->user_unique)->whereId($id)->first();
        try {
            if($academic->delete() ){
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Record has been deleted'
                    ], 200);
                } else {
                    return redirect()->back()->with('success', 'Record has been deleted');
                }
                
            } else {
                return redirect()->back()->with('warning', 'Record has been deleted');
            
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('errors', $e->getMessage());
        }
    }

    protected  function getAcademicRule()
    {
        return [
            'educational_level' => 'required',
            'country_obtained' => 'required',
            'discipline' => 'required',
            'course_of_study' => 'required',
            'start_year' => 'required|date',
            'end_year' => 'required|date',
            'name_of_institution' => 'required',
            'degree_obtained' => 'required'
        ];
    }

    protected function getAcademicData () {
        $user = auth()->user()->user_unique;
        if(request()->file('files')){

            request()->validate(['files' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048']);
    
            $name = request()->file('files');
            $file = $name->getClientOriginalName();
            $filename = uniqid().'_'.time().'_'.$file;
            $filepath = 'uploads/individual/academic/'.$filename;
        }
        $file = request()->file('files') ? $filepath : null;

        return [
            'educational_level' => request()->educational_level,
            'user_unique' => $user,
            'country_obtained' => request()->country_obtained,
            'discipline' => request()->discipline,
            'course_of_study' => request()->course_of_study,
            'start_year' => request()->start_year,
            'end_year' => request()->end_year,
            'name_of_institution' => request()->name_of_institution,
            'degree_obtained' => request()->degree_obtained,
            'files' => $file,
        ];
    }
}
