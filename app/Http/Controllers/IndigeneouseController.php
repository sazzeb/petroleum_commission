<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\IndigenousBankPlan;

class IndigeneouseController extends Controller
{

    public function __construct (){
        $this->middleware('auth');
    }
    
    public function index()
    {
        
    }

    
    public function create()
    {
        //
    }

    
    public function store()
    {
        request()->validate([
            'name_of_banks' => 'required|string',
            'office_location' => 'required|string',
            'list_of_service' => 'required'
        ]);

        $data = request()->all();
        $data['user_unique'] = auth()->user()->user_unique;

        try {
            if($plan = IndigenousBankPlan::create($data)){
                return response()->json(['data' => $plan], 200);
            }
        } catch (Exception $e){
            return response()->json(['errors' => $e->getMessage()]);
        }

    }

    
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update($id)
    {
        request()->validate([
            'name_of_banks' => 'required|string',
            'office_location' => 'required|string',
            'list_of_service' => 'required'
        ]);

        $data = request()->all();
        $plan = IndigenousBankPlan::whereUserUnique(auth()->user()->user_unique)->whereId($id)->first();
        try {
            if($plan->update($data)){
                return response()->json(['data' => $plan->fresh()], 200);
            }
        } catch (Exception $e){
            return response()->json(['errors' => $e->getMessage()]);
        }

    }

    
    public function destroy($id)
    {
        //
    }
}
