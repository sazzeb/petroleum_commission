<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\InsuranceServicePlan;

class InsuranceServiceController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $user = auth()->user()->user_unique;

        request()->validate([
            'name_of_insurance_provider' => 'required|string',
            'office_location' => 'required|string',
            'asset_issued' => 'required|string',
            'sum_issued' => 'required|integer',
            'insurance_cover' => 'required|string',
            'insurance_broker' => 'required|string',
            'name_of_reinsurance_service_provider' => 'required',
            'asset_reinsured' => 'required',
            'sum_reinsured' => 'required|integer',
            'reason_for_omission' => 'string'
        ]);

        try {
            $data = $this->getData();
            if($newData = InsuranceServicePlan::create($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'data' => $newData,
                    ], 201);
                }
            }
        } catch (Exception $e){
            return response()->json(['errors' => $e->getMessage()], 403);
        }

    }

    
    public function show($id)
    {
        request()->validate([
            'files' => 'required',
            'files.*' => 'mimes:jpeg,png,jpg,txt,pdf,doc,docx|max:2048'
 
        ]);

        if(request()->hasFile('files')) {
            $auth = auth()->user()->user_unique;
            $company  = InsuranceServicePlan::whereUserUnique($auth)->whereId($id)->first();

            try {
                foreach(request()->file('files') as $file){
                    $name = $file->getClientOriginalName();
                    $dir = 'uploads/insured/details/';
                    $filename = uniqid().'_'.time().'_'.$name;
                    $move = $file->move($dir, $filename);
                    $filepath = 'uploads/insured/details/'.$filename;
                    if($move){
                        $company->image()->create([
                            'user_unique' => $auth,
                            'file' => $filepath
                        ]);
                    }
                    
                }
                if(request()->expectsJson()){
                    return response()->json([
                        'message' => 'Congratulation! file has been added',
                        'data' => $company->image,
                    ], 201);
                }
            } catch (Exception $e) {
                return response()->json([
                            'error' => $e->getMessage
                        ], 403);
            }
        }
    }

    
    public function edit($id)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        $user = auth()->user()->user_unique;

        request()->validate([
            'name_of_insurance_provider' => 'required|string',
            'office_location' => 'required|string',
            'asset_issued' => 'required|string',
            'sum_issued' => 'required|integer',
            'insurance_cover' => 'required|string',
            'insurance_broker' => 'required|string',
            'name_of_reinsurance_service_provider' => 'required',
            'asset_reinsured' => 'required',
            'sum_reinsured' => 'required|integer',
            'reason_for_omission' => 'string'
        ]);

        $insured = InsuranceServicePlan::whereUserUnique($user)->whereId($id)->first();

        try {
            $data = $this->getData();

            if($insured->update($data)){
                if(request()->expectsJson()){
                    return response()->json([
                        'data' => $insured->fresh(),
                    ], 201);
                }
            }
        } catch (Exception $e){
            return response()->json(['errors' => $e->getMessage()], 403);
        }
    }

    
    public function destroy($id)
    {
        //
    }

    protected function getData (){
        $ret = request()->reason_for_omission ? request()->reason_for_omission : null;
        return [
            'user_unique' => auth()->user()->user_unique,
            'name_of_insurance_provider' => request()->name_of_insurance_provider,
            'office_location' => request()->office_location,
            'asset_issued' => request()->asset_issued,
            'sum_issued' => request()->sum_issued,
            'insurance_cover' => request()->insurance_cover,
            'insurance_broker' => request()->insurance_broker,
            'name_of_reinsurance_service_provider' => request()->name_of_reinsurance_service_provider,
            'asset_reinsured' => request()->asset_reinsured,
            'sum_reinsured' => request()->sum_reinsured,
            'reason_for_omission' => $ret
        ];
    }
}
