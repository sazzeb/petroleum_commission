<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class FinancialServicePlan extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->morphMany(FileUpload::class, 'model');
    }


    public function setDateOfEngagementAttribute($value)
    {
        $this->attributes['date_of_engagement'] = Carbon::parse($value);
    }

    public function setIndigenousBankDateOfEngagementAttribute($value)
    {
        $this->attributes['indigenous_bank_date_of_engagement'] = Carbon::parse($value);
    }

}

