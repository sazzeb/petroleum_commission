<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CompanyDetail extends Model
{
    protected $guarded = [];

    protected $dates = ['date_of_incorporation', 'date_of_commencement_of_service'];

    public function setDateOfIncorporationAttribute($value)
    {
        $this->attributes['date_of_incorporation'] = Carbon::parse($value);
    }

    public function setDateOfCommencementOfServiceAttribute($value)
    {
        $this->attributes['date_of_commencement_of_service'] = Carbon::parse($value);
    }

    public function image()
    {
        return $this->morphMany(FileUpload::class, 'model');
    }
}
