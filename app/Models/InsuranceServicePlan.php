<?php

namespace App\Models;
use Carbon\Carbon;


use Illuminate\Database\Eloquent\Model;

class InsuranceServicePlan extends Model
{
    protected $guarded = [];

    public function image()
    {
        return $this->morphMany(FileUpload::class, 'model');
    }
}
