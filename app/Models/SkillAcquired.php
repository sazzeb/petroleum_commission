<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SkillAcquired extends Model
{
    protected $guarded = [];
}
