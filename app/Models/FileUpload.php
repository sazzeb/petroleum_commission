<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    protected $guarded = [];

    public function model() {
        return $this->morphTo();
    }

    public function getFileAttribute ($file){
        if ($file){
            return asset($file);
        }
    }
}
