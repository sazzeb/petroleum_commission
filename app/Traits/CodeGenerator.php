<?php

namespace App\Traits;

use Keygen\Keygen;

trait CodeGenerator {
    private function generateKey () {
        return Keygen::numeric(9)->prefix(mt_rand(1,9))
            ->prefix($this->codePrefix)
            ->generate(true);
    }

    public function generateUniqueCode($model = null, $column = 'user_unique')
    {
        $code = Keygen::numeric(7)->prefix(mt_rand(1, 9))->generate(true);


        $query = $model ? $model::where($column, $code) :
            self::where($column, $code);

        // Ensure Code does not exist
        // Generate new one if Code already exists
        while ($query->count() > 0) {
            $code = Keygen::numeric(7)->prefix(mt_rand(1, 9))->generate(true);
        }

        return $code;
    }

    public static function generateCode()
    {
        return Keygen::numeric(5)->mt_rand(1, 9)
            ->generate(true);
    }
}
