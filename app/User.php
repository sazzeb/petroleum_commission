<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
// use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\CodeGenerator;

class User extends Authenticatable
{
    use Notifiable, CodeGenerator;

    protected static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->user_unique = $model->generateUniqueCode();
        });
    }

    


    protected $guarded = [];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
