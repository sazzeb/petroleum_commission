<?php
Route::group(["prefix" => "operator","namespace" => "Operator"],function(){
    Route::get('/dashboard','OperatorController@dashboard')->name('operator.dashboard');
    Route::get('/company-details', 'CompanyDetailController@index')->name('company-details.index');
    Route::get('/succession-plan', 'SuccessPlanController@index')->name('success-plan.index');
    Route::get('/financial-plan', 'FinancialServicesController@index')->name('financial-services.index');
    Route::get('/legal-service-plan', 'LegalServicesController@index')->name('legal-services-plan.index');
    Route::get('/insurance-service-plan', 'InsuranceServicePlanController@index')->name('insurance-services-plan.index');
    Route::get('/technology-transfer-program', 'TechnologyTransferProgramController@index')->name('technology-transfer-program');
    Route::get('/research-development-plan', 'ResearchDevelopmentController@index')->name('research-development-plan');
    Route::get('/indigenous-plan', 'IndigenousPlanController@index')->name('indigenous-plan');
});
Route::group(["prefix" => "service-company","namespace" => "Operator"],function(){
    Route::get('/dashboard','OperatorController@dashboard')->name('operator.dashboard');
    Route::get('/company-details', 'CompanyDetailController@index')->name('company-details.index');
    Route::get('/succession-plan', 'SuccessPlanController@index')->name('success-plan.index');
    Route::get('/financial-plan', 'FinancialServicesController@index')->name('financial-services.index');
    Route::get('/legal-service-plan', 'LegalServicesController@index')->name('legal-services-plan.index');
    Route::get('/insurance-service-plan', 'InsuranceServicePlanController@index')->name('insurance-services-plan.index');
    Route::get('/technology-transfer-program', 'TechnologyTransferProgramController@index')->name('technology-transfer-program');
    Route::get('/research-development-plan', 'ResearchDevelopmentController@index')->name('research-development-plan');
    Route::get('/indigenous-plan', 'IndigenousPlanController@index')->name('indigenous-plan');
});
Route::get('/employee-details','EmployeeDetailController@index')->name('operator.employee');
Route::group(["prefix" => "individual","namespace" => "Individual"],function(){
    Route::get('/dashboard','IndividualController@dashboard')->name('individual.dashboard');
    Route::get('/academic-details','AcademicRecordController@index')->name('individual.academic-details');
    Route::get('/job-experience','JobExperienceRecordController@index')->name('individual.job-experience');
    Route::get('/skills-acquired','SkillsRecordController@index')->name('individual.job-experience');
 });
 
Auth::routes();

Route::group(['namespace' => 'Api'], function() {
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('/register', 'RegistrationController@store')->name('register');
        Route::post('/company', 'RegistrationController@create')->name('register.company');
        Route::post('/login', 'RegistrationController@login')->name('login');
        Route::get('/logout-user', 'RegistrationController@logout');
    });
    Route::get('/users', 'IndividualController@index')->name('users.individual');
    Route::post('/users','IndividualController@update_user')->name('user.update');
    Route::get('/academic-details','IndividualController@show')->name('academic.index');
    Route::get('/academic/{id}','IndividualController@edit')->name('academic.show');
    Route::post('/academic','IndividualController@store')->name('academic.store');
    Route::post('/academic/{id}','IndividualController@update')->name('academic.update');
    Route::get ('/academic/{id}','IndividualController@destroy')->name('academic.destroy');

    
    Route::get('/job-experience', 'SkillIndividualController@index')->name('jobs.index');
    Route::get('/skills-acquired', 'SkillIndividualController@all_skill')->name('skills.index');
    Route::get('/jobs/{id}', 'SkillIndividualController@show')->name('jobs.show');
    Route::get('/skills/{id}', 'SkillIndividualController@one_skill')->name('skills.show');
    Route::post('/jobs', 'SkillIndividualController@store')->name('jobs.store');
    Route::post('/skills', 'SkillIndividualController@add_skill')->name('skills.store');
    Route::post('/jobs/{id}', 'SkillIndividualController@update')->name('jobs.update');
    Route::post('/skills-update/{id}', 'SkillIndividualController@update_skill')->name('skills.update');
    Route::get('/jobs/{id}', 'SkillIndividualController@destroy')->name('jobs.destroy');
    Route::get('/skills/{id}', 'SkillIndividualController@remove_skill')->name('skills.destroy');
});


Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();

    Route::get('/companydetail', 'CompanyDetailController@index')->name('company.index');
    Route::get('/companydetail/{id}', 'CompanyDetailController@show')->name('company.show');
    Route::get('/companydetail/perview/{id}', 'CompanyDetailController@edit')->name('company.preview');
    Route::post('/companydetail/post', 'CompanyDetailController@store')->name('company.store');
    Route::post('/companydetail/{id}', 'CompanyDetailController@update')->name('company.update'); 
    Route::delete('/companydetail/{id}', 'CompanyDetailController@destroy')->name('company.destroy');

    Route::get('operator/employee', 'EmploymentPlancontroller@index')->name('employee.index');
    Route::get('/employee/{id}', 'EmploymentPlancontroller@show')->name('employee.show');
    Route::get('/employee/preview/{id}', 'EmploymentPlancontroller@edit')->name('employee.preview');
    Route::post('/employee', 'EmploymentPlancontroller@store')->name('employee.store');
    Route::post('/employee/{id}', 'EmploymentPlancontroller@update')->name('employee.update');
    Route::delete('/employee/{id}', 'EmploymentPlancontroller@destroy')->name('employee.destroy');

    Route::get('/financial', 'FinancialPlancontroller@index')->name('financial.index');
    Route::get('/financial/{id}', 'FinancialPlancontroller@show')->name('financial.show');
    Route::get('/financial/preview/{id}', 'FinancialPlancontroller@edit')->name('financial.preview');
    Route::post('/financial/post', 'FinancialPlancontroller@store')->name('financial.store');
    Route::post('/financial/{id}', 'FinancialPlancontroller@update')->name('financial.update');
    Route::delete('/financial/{id}', 'FinancialPlancontroller@destroy')->name('financial.destroy');

    Route::get('/succession', 'SuccessionPlancontroller@index')->name('succession.index');
    Route::post('/succession/upload/{id}', 'SuccessionPlancontroller@show')->name('succession.show');
    Route::get('/succession/preview/{id}', 'SuccessionPlancontroller@edit')->name('succession.preview');
    Route::post('/succession/post', 'SuccessionPlancontroller@store')->name('succession.store');
    Route::post('/succession/{id}', 'SuccessionPlancontroller@update')->name('succession.update');
    Route::delete('/succession/{id}', 'SuccessionPlancontroller@destroy')->name('succession.destroy');

    Route::get('/legal', 'LegalPlancontroller@index')->name('legal.index');
    Route::get('/legal/{id}', 'LegalPlancontroller@show')->name('legal.show');
    Route::get('/legal/preview/{id}', 'LegalPlancontroller@edit')->name('legal.preview');
    Route::post('/legal/post', 'LegalPlancontroller@store')->name('legal.store');
    Route::post('/legal/{id}', 'LegalPlancontroller@update')->name('legal.update');
    Route::delete('/legal/{id}', 'LegalPlancontroller@destroy')->name('legal.destroy');

    Route::get('/insurance', 'InsuranceServiceController@index')->name('insure.index');
    Route::post('/insurance/upload/{id}', 'InsuranceServiceController@show')->name('insure.show');
    Route::get('/insurance/preview/{id}', 'InsuranceServiceController@edit')->name('insure.preview');
    Route::post('/insurance/post', 'InsuranceServiceController@store')->name('insure.store');
    Route::post('/insurance/{id}', 'InsuranceServiceController@update')->name('insure.update');
    Route::delete('/insurance/{id}', 'InsuranceServiceController@destroy')->name('insure.destroy');

    Route::get('/technology', 'TechnologyTransferController@index')->name('tech.index');
    Route::post('/technology/upload/{id}', 'TechnologyTransferController@show')->name('tech.show');
    Route::get('/technology/preview/{id}', 'TechnologyTransferController@edit')->name('tech.preview');
    Route::post('/technology/post', 'TechnologyTransferController@store')->name('tech.store');
    Route::post('/technology/{id}', 'TechnologyTransferController@update')->name('tech.update');
    Route::delete('/technology/{id}', 'TechnologyTransferController@destroy')->name('tech.destroy');

    Route::get('/research', 'ResearhAndDevelopmentController@index')->name('research.index');
    Route::post('/research/upload/{id}', 'ResearhAndDevelopmentController@show')->name('research.show');
    Route::get('/research/preview/{id}', 'ResearhAndDevelopmentController@edit')->name('research.preview');
    Route::post('/research/post', 'ResearhAndDevelopmentController@store')->name('research.store');
    Route::post('/research/{id}', 'ResearhAndDevelopmentController@update')->name('research.update');
    Route::delete('/research/{id}', 'ResearhAndDevelopmentController@destroy')->name('research.destroy');

    // for payment

    Route::get('/payment', 'PaymentController@index')->name('paystack.payment');;
    Route::post('/payment', 'PaymentController@store')->name('paystack.payment');
    Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');

    Route::post('/indigenoues/post', 'IndigeneouseController@store')->name('indigenoues.store');
    Route::post('/indigenoues/{id}', 'IndigeneouseController@update')->name('indigenoues.update');
