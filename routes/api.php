<?php

use App\Models\EmploymmentPlan;
use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1', 'namespace' => 'Api'], function() {
    Route::group(['namespace' => 'Auth'], function () {
        Route::post('/register', 'RegistrationController@store')->name('register');
        Route::post('/company', 'RegistrationController@create');
        Route::post('/login', 'RegistrationController@login');
    });

    Route::get('/users', 'IndividualController@index');
    Route::post('/users','IndividualController@update_user');
    Route::get('/academic','IndividualController@show');
    Route::get('/academic/{id}','IndividualController@edit');
    Route::post('/academic','IndividualController@store');
    Route::patch('/academic','IndividualController@update');
    Route::delete('/academic/{id}','IndividualController@destroy');

    
    Route::get('/jobs', 'SkillIndividualController@index');
    Route::get('/skills', 'SkillIndividualController@all_skill');
    Route::get('/jobs/{id}', 'SkillIndividualController@show');
    Route::get('/skills/{id}', 'SkillIndividualController@one_skill');
    Route::post('/jobs', 'SkillIndividualController@store');
    Route::post('/skills', 'SkillIndividualController@add_skill');
    Route::patch('/jobs/{id}', 'SkillIndividualController@update');
    Route::patch('/skills/{id}', 'SkillIndividualController@update_skill');
    Route::delete('/jobs/{id}', 'SkillIndividualController@destroy');
    Route::delete('/skills/{id}', 'SkillIndividualController@remove_skill');

});

Route::get('/companydetail', 'CompanyDetailController@index')->name('company.index');
Route::get('/companydetail/{id}', 'CompanyDetailController@show')->name('company.show');
Route::get('/companydetail/perview/{id}', 'CompanyDetailController@edit')->name('company.preview');
Route::post('/companydetail', 'CompanyDetailController@store')->name('company.store');
Route::post('/companydetail/{id}', 'CompanyDetailController@update')->name('company.update'); 
Route::delete('/companydetail/{id}', 'CompanyDetailController@destroy')->name('company.destroy');

Route::get('/employee', 'EmploymentPlancontroller@index')->name('employee.index');
Route::get('/employee/{id}', 'EmploymentPlancontroller@show')->name('employee.show');
Route::get('/employee/preview/{id}', 'EmploymentPlancontroller@edit')->name('employee.preview');
Route::post('/employee', 'EmploymentPlancontroller@store')->name('employee.store');
Route::post('/employee/{id}', 'EmploymentPlancontroller@update')->name('employee.update'); 
Route::delete('/employee/{id}', 'EmploymentPlancontroller@destroy')->name('employee.destroy');

Route::get('/financial', 'FinancialPlancontroller@index')->name('financial.index');
Route::get('/financial/{id}', 'FinancialPlancontroller@show')->name('financial.show');
Route::get('/financial/preview/{id}', 'FinancialPlancontroller@edit')->name('financial.preview');
Route::post('/financial', 'FinancialPlancontroller@store')->name('financial.store');
Route::post('/financial/{id}', 'FinancialPlancontroller@update')->name('financial.update');
Route::delete('/financial/{id}', 'FinancialPlancontroller@destroy')->name('financial.destroy');

Route::get('/succession', 'SuccessionPlancontroller@index')->name('succession.index');
Route::get('/succession/{id}', 'SuccessionPlancontroller@show')->name('succession.show');
Route::get('/succession/preview/{id}', 'SuccessionPlancontroller@edit')->name('succession.preview');
Route::post('/succession', 'SuccessionPlancontroller@store')->name('succession.store');
Route::post('/succession/{id}', 'SuccessionPlancontroller@update')->name('succession.update');
Route::delete('/succession/{id}', 'SuccessionPlancontroller@destroy')->name('succession.destroy');


Route::get('/legal', 'LegalPlancontroller@index')->name('legal.index');
Route::get('/legal/{id}', 'LegalPlancontroller@show')->name('legal.show');
Route::get('/legal/preview/{id}', 'LegalPlancontroller@edit')->name('legal.preview');
Route::post('/legal', 'LegalPlancontroller@store')->name('legal.store');
Route::post('/legal/{id}', 'LegalPlancontroller@update')->name('legal.update');
Route::delete('/legal/{id}', 'LegalPlancontroller@destroy')->name('legal.destroy');
